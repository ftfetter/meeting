FROM openjdk:8-jdk-alpine

EXPOSE 8080

ARG JAR_FILE=build/libs/meeting-0.0.1-SNAPSHOT.jar

ADD ${JAR_FILE} meeting.jar

ENTRYPOINT ["java","-jar","/meeting.jar"]