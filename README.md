# Meeting App

## Running instructions

To run the application, first you need to go to the project root folder and please execute  
`./gradlew clean build`  
this will run the application tests and build it.

Then, with **Docker** and **Docker Compose** installed, run  
`docker-compose build`  
to prepare all the necessary containers. 

So, it's time to run that application. Just execute  
`docker-compose up -d`  
and _voilà_! The application is up and running!

Meeting app will run on `localhost:8080` and MongoDB on `localhost:27017`.  

To check if it's all ok with the application, please access `localhost:8080/actuator/health`.

---
## Future Features (*Things planned but not coded*)
- An scheduler that will receive the `dueDateTime` and `id` of a voting session and, when the time comes, execute
`getSessionResult` method from *SessionService*, and then publishes the result on a RabbitMQ queue.
- Load tests with **Gatling** to validate the system capacity.
- **Swagger** documentation. 