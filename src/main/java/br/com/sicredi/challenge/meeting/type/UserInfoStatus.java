package br.com.sicredi.challenge.meeting.type;

public enum  UserInfoStatus {

    ABLE_TO_VOTE,
    UNABLE_TO_VOTE;
}
