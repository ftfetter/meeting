package br.com.sicredi.challenge.meeting.model;

import java.time.LocalDateTime;

public class Session {

    private String id;
    private String topicId;
    private LocalDateTime dueDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTopicId() {
        return topicId;
    }

    public void setTopicId(String topicId) {
        this.topicId = topicId;
    }

    public LocalDateTime getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDateTime dueDate) {
        this.dueDate = dueDate;
    }


    public static final class SessionBuilder {
        private String id;
        private String topicId;
        private LocalDateTime dueDate;

        private SessionBuilder() {
        }

        public static SessionBuilder aSession() {
            return new SessionBuilder();
        }

        public SessionBuilder withId(String id) {
            this.id = id;
            return this;
        }

        public SessionBuilder withTopicId(String topicId) {
            this.topicId = topicId;
            return this;
        }

        public SessionBuilder withDueDate(LocalDateTime dueDate) {
            this.dueDate = dueDate;
            return this;
        }

        public Session build() {
            Session session = new Session();
            session.setId(id);
            session.setTopicId(topicId);
            session.setDueDate(dueDate);
            return session;
        }
    }

    @Override
    public String toString() {
        return "Session{" +
                "id='" + id + '\'' +
                ", topicId='" + topicId + '\'' +
                ", dueDate=" + dueDate +
                '}';
    }
}
