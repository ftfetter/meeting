package br.com.sicredi.challenge.meeting.model;

import br.com.sicredi.challenge.meeting.type.VoteType;

public class Vote {

    private String id;
    private String sessionId;
    private String associateId;
    private VoteType value;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getAssociateId() {
        return associateId;
    }

    public void setAssociateId(String associateId) {
        this.associateId = associateId;
    }

    public VoteType getValue() {
        return value;
    }

    public void setValue(VoteType value) {
        this.value = value;
    }


    public static final class VoteBuilder {
        private String id;
        private String sessionId;
        private String associateId;
        private VoteType value;

        private VoteBuilder() {
        }

        public static VoteBuilder aVote() {
            return new VoteBuilder();
        }

        public VoteBuilder withId(String id) {
            this.id = id;
            return this;
        }

        public VoteBuilder withSessionId(String sessionId) {
            this.sessionId = sessionId;
            return this;
        }

        public VoteBuilder withAssociateId(String associateId) {
            this.associateId = associateId;
            return this;
        }

        public VoteBuilder withValue(VoteType value) {
            this.value = value;
            return this;
        }

        public Vote build() {
            Vote vote = new Vote();
            vote.setId(id);
            vote.setSessionId(sessionId);
            vote.setAssociateId(associateId);
            vote.setValue(value);
            return vote;
        }
    }

    @Override
    public String toString() {
        return "Vote{" +
                "id='" + id + '\'' +
                ", sessionId='" + sessionId + '\'' +
                ", associateId='" + associateId + '\'' +
                ", value=" + value +
                '}';
    }
}
