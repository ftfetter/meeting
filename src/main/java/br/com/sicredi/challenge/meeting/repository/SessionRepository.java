package br.com.sicredi.challenge.meeting.repository;

import br.com.sicredi.challenge.meeting.entity.SessionEntity;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SessionRepository extends ReactiveMongoRepository<SessionEntity, String> {
}
