package br.com.sicredi.challenge.meeting.controller.v1.request;

public class SessionRequest {

    private String topicId;
    private Long durationInMinutes;

    public String getTopicId() {
        return topicId;
    }

    public void setTopicId(String topicId) {
        this.topicId = topicId;
    }

    public Long getDurationInMinutes() {
        return durationInMinutes;
    }

    public void setDurationInMinutes(Long durationInMinutes) {
        this.durationInMinutes = durationInMinutes;
    }

    @Override
    public String toString() {
        return "SessionRequest{" +
                "topicId='" + topicId + '\'' +
                ", durationInMinutes=" + durationInMinutes +
                '}';
    }
}
