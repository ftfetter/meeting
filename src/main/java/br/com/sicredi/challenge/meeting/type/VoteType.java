package br.com.sicredi.challenge.meeting.type;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.stream.Stream;

public enum VoteType {

    YES,
    NO;

    public static VoteType getVoteByValue(String value) {
        return Stream.of(VoteType.values())
                .filter(voteType -> voteType.name().equalsIgnoreCase(value))
                .findAny()
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid vote value."));
    }
}
