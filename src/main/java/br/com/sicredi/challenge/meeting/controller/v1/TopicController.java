package br.com.sicredi.challenge.meeting.controller.v1;

import br.com.sicredi.challenge.meeting.controller.v1.request.TopicRequest;
import br.com.sicredi.challenge.meeting.controller.v1.response.TopicResponse;
import br.com.sicredi.challenge.meeting.mapper.TopicMapper;
import br.com.sicredi.challenge.meeting.model.Topic;
import br.com.sicredi.challenge.meeting.service.TopicService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api/v1/meeting/topics")
public class TopicController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private TopicService topicService;

    public TopicController(TopicService topicService) {
        this.topicService = topicService;
    }

    @PostMapping
    public Mono<TopicResponse> createTopic(@RequestBody TopicRequest request) {
        logger.info("Request to create a new Topic with body: {}", request);
        Topic newTopic = TopicMapper.fromRequest(request);
        return topicService.createTopic(newTopic)
                .map(TopicMapper::toResponse)
                .doOnSuccess(response -> logger.info("Topic created successfully: {}", response));
    }
}
