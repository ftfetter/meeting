package br.com.sicredi.challenge.meeting.controller.v1;

import br.com.sicredi.challenge.meeting.controller.v1.request.AssociateRequest;
import br.com.sicredi.challenge.meeting.controller.v1.response.AssociateResponse;
import br.com.sicredi.challenge.meeting.mapper.AssociateMapper;
import br.com.sicredi.challenge.meeting.model.Associate;
import br.com.sicredi.challenge.meeting.service.AssociateService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api/v1/meeting/associates")
public class AssociateController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private AssociateService associateService;

    public AssociateController(AssociateService associateService) {
        this.associateService = associateService;
    }

    @PostMapping
    public Mono<AssociateResponse> createAssociate(@RequestBody AssociateRequest request) {
        logger.info("Request to create a new Associate with body: {}", request);
        Associate newAssociate = AssociateMapper.fromRequest(request);
        return associateService.createAssociate(newAssociate)
                .map(AssociateMapper::toResponse)
                .doOnSuccess(response -> logger.info("Associate created successfully: {}", response));
    }
}
