package br.com.sicredi.challenge.meeting.client;

import br.com.sicredi.challenge.meeting.client.response.UserInfoClientResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Mono;

import java.util.function.Function;

@Component
public class UserInfoClient {

    private WebClient webClient;

    public UserInfoClient(@Value("${client.userinfo.url}") String urlBase) {
        this.webClient = WebClient.create(urlBase);
    }

    public Mono<UserInfoClientResponse> getUserInfo(String taxId) {
        return webClient.get()
                .uri("/users/{taxId}", taxId)
                .retrieve()
                .onStatus(httpStatus -> httpStatus == HttpStatus.NOT_FOUND, handleError())
                .bodyToMono(UserInfoClientResponse.class);
    }

    private Function<ClientResponse, Mono<? extends Throwable>> handleError() {
        return clientResponse -> Mono.error(new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid TaxID."));
    }
}
