package br.com.sicredi.challenge.meeting.mapper;

import br.com.sicredi.challenge.meeting.controller.v1.response.SessionResultResponse;
import br.com.sicredi.challenge.meeting.model.SessionResult;

public final class SessionResultMapper {

    private SessionResultMapper() {
    }

    public static SessionResultResponse toResponse(SessionResult sessionResult) {
        return SessionResultResponse.SessionResultResponseBuilder.aSessionResultResponse()
                .withTopicId(sessionResult.getTopicId())
                .withSessionId(sessionResult.getSessionId())
                .withTotalVotes(sessionResult.getTotalVotes())
                .withResult(sessionResult.getResult())
                .build();
    }
}
