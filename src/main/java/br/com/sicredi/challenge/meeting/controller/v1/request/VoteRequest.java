package br.com.sicredi.challenge.meeting.controller.v1.request;

public class VoteRequest {

    private String associateId;
    private String value;

    public String getAssociateId() {
        return associateId;
    }

    public void setAssociateId(String associateId) {
        this.associateId = associateId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "VoteRequest{" +
                "associateId='" + associateId + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
