package br.com.sicredi.challenge.meeting.model;

public class SessionResult {

    private String sessionId;
    private String topicId;
    private Integer totalVotes;
    private String result;

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getTopicId() {
        return topicId;
    }

    public void setTopicId(String topicId) {
        this.topicId = topicId;
    }

    public Integer getTotalVotes() {
        return totalVotes;
    }

    public void setTotalVotes(Integer totalVotes) {
        this.totalVotes = totalVotes;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }


    public static final class SessionResultsBuilder {
        private String sessionId;
        private String topicId;
        private Integer totalVotes;
        private String result;

        private SessionResultsBuilder() {
        }

        public static SessionResultsBuilder aSessionResults() {
            return new SessionResultsBuilder();
        }

        public SessionResultsBuilder withSessionId(String sessionId) {
            this.sessionId = sessionId;
            return this;
        }

        public SessionResultsBuilder withTopicId(String topicId) {
            this.topicId = topicId;
            return this;
        }

        public SessionResultsBuilder withTotalVotes(Integer totalVotes) {
            this.totalVotes = totalVotes;
            return this;
        }

        public SessionResultsBuilder withResult(String result) {
            this.result = result;
            return this;
        }

        public SessionResult build() {
            SessionResult sessionResult = new SessionResult();
            sessionResult.setSessionId(sessionId);
            sessionResult.setTopicId(topicId);
            sessionResult.setTotalVotes(totalVotes);
            sessionResult.setResult(result);
            return sessionResult;
        }
    }

    @Override
    public String toString() {
        return "SessionResults{" +
                "sessionId='" + sessionId + '\'' +
                ", topicId='" + topicId + '\'' +
                ", totalVotes=" + totalVotes +
                ", result='" + result + '\'' +
                '}';
    }
}
