package br.com.sicredi.challenge.meeting.model;

public class Topic {

    private String id;
    private String name;
    private String description;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public static final class TopicBuilder {
        private String id;
        private String name;
        private String description;

        private TopicBuilder() {
        }

        public static TopicBuilder aTopic() {
            return new TopicBuilder();
        }

        public TopicBuilder withId(String id) {
            this.id = id;
            return this;
        }

        public TopicBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public TopicBuilder withDescription(String description) {
            this.description = description;
            return this;
        }

        public Topic build() {
            Topic topic = new Topic();
            topic.setId(id);
            topic.setName(name);
            topic.setDescription(description);
            return topic;
        }
    }

    @Override
    public String toString() {
        return "Topic{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
