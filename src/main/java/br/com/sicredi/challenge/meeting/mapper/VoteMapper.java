package br.com.sicredi.challenge.meeting.mapper;

import br.com.sicredi.challenge.meeting.controller.v1.request.VoteRequest;
import br.com.sicredi.challenge.meeting.controller.v1.response.VoteResponse;
import br.com.sicredi.challenge.meeting.entity.VoteEntity;
import br.com.sicredi.challenge.meeting.model.Vote;
import br.com.sicredi.challenge.meeting.type.VoteType;

public final class VoteMapper {

    private VoteMapper() {
    }

    public static Vote fromRequest(String sessionId, VoteRequest request) {
        return Vote.VoteBuilder.aVote()
                .withSessionId(sessionId)
                .withAssociateId(request.getAssociateId())
                .withValue(VoteType.getVoteByValue(request.getValue()))
                .build();
    }

    public static VoteEntity toEntity(Vote vote) {
        return VoteEntity.VoteEntityBuilder.aVoteEntity()
                .withSessionId(vote.getSessionId())
                .withAssociateId(vote.getAssociateId())
                .withValue(vote.getValue())
                .build();
    }

    public static Vote fromEntity(VoteEntity entity) {
        return Vote.VoteBuilder.aVote()
                .withId(entity.getId())
                .withSessionId(entity.getSessionId())
                .withAssociateId(entity.getAssociateId())
                .withValue(entity.getValue())
                .build();
    }

    public static VoteResponse toResponse(Vote vote) {
        return VoteResponse.VoteResponseBuilder.aVoteResponse()
                .withId(vote.getId())
                .withSessionId(vote.getSessionId())
                .withAssociateId(vote.getAssociateId())
                .withValue(vote.getValue().name())
                .build();
    }
}
