package br.com.sicredi.challenge.meeting.mapper;

import br.com.sicredi.challenge.meeting.controller.v1.request.TopicRequest;
import br.com.sicredi.challenge.meeting.controller.v1.response.TopicResponse;
import br.com.sicredi.challenge.meeting.entity.TopicEntity;
import br.com.sicredi.challenge.meeting.model.Topic;

public final class TopicMapper {

    private TopicMapper() {
    }

    public static Topic fromRequest(TopicRequest request) {
        return Topic.TopicBuilder.aTopic()
                .withName(request.getName())
                .withDescription(request.getDescription())
                .build();
    }

    public static TopicResponse toResponse(Topic topic) {
        return TopicResponse.TopicResponseBuilder.aTopicResponse()
                .withId(topic.getId())
                .withName(topic.getName())
                .withDescription(topic.getDescription())
                .build();
    }

    public static Topic fromEntity(TopicEntity entity) {
        return Topic.TopicBuilder.aTopic()
                .withId(entity.getId())
                .withName(entity.getName())
                .withDescription(entity.getDescription())
                .build();
    }

    public static TopicEntity toEntity(Topic topic) {
        return TopicEntity.TopicEntityBuilder.aTopicEntity()
                .withName(topic.getName())
                .withDescription(topic.getDescription())
                .build();
    }
}
