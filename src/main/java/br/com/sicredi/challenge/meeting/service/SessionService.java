package br.com.sicredi.challenge.meeting.service;

import br.com.sicredi.challenge.meeting.entity.SessionEntity;
import br.com.sicredi.challenge.meeting.entity.VoteEntity;
import br.com.sicredi.challenge.meeting.mapper.SessionMapper;
import br.com.sicredi.challenge.meeting.mapper.VoteMapper;
import br.com.sicredi.challenge.meeting.model.Session;
import br.com.sicredi.challenge.meeting.model.SessionResult;
import br.com.sicredi.challenge.meeting.model.Vote;
import br.com.sicredi.challenge.meeting.repository.SessionRepository;
import br.com.sicredi.challenge.meeting.repository.VoteRepository;
import br.com.sicredi.challenge.meeting.type.VoteType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;

@Service
public class SessionService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private SessionRepository sessionRepository;
    private VoteRepository voteRepository;
    private AssociateService associateService;

    public SessionService(SessionRepository sessionRepository, VoteRepository voteRepository, AssociateService associateService) {
        this.sessionRepository = sessionRepository;
        this.voteRepository = voteRepository;
        this.associateService = associateService;
    }

    public Mono<Session> createSession(Session newSession) {
        logger.info("Creating new session: {}", newSession);
        SessionEntity newSessionEntity = SessionMapper.toEntity(newSession);
        return sessionRepository.save(newSessionEntity)
                .map(SessionMapper::fromEntity);
    }

    public Mono<Vote> registerSessionVote(Vote vote) {
        return Mono.zip(
                isSessionOpen(vote.getSessionId()),
                isAssociateAlreadyRegistered(vote.getAssociateId()),
                associateService.isAbleToVote(vote.getAssociateId()),
                isAssociateAlreadyVotedForSession(vote.getAssociateId(), vote.getSessionId()))
                .filter(tuple -> checkConditionsToVote(tuple.getT1(), tuple.getT2(), tuple.getT3(), tuple.getT4()))
                .doOnNext(tuple -> logger.info("Registering new vote: {}", vote))
                .map(sessionEntity -> vote)
                .map(VoteMapper::toEntity)
                .flatMap(voteEntity -> voteRepository.save(voteEntity))
                .map(VoteMapper::fromEntity);
    }

    public Mono<SessionResult> getSessionResults(String sessionId) {
        logger.info("Counting session results for ID: {}", sessionId);
        return Mono.zip(
                verifySessionClosedAndReturn(sessionId),
                voteRepository.findAllBySessionId(sessionId).collectList())
                .map(tuple -> countSessionResults(tuple.getT1(), tuple.getT2()));
    }

    private Mono<Boolean> isSessionOpen(String sessionId) {
        logger.info("Checking that the session is open for voting.");
        return sessionRepository.findById(sessionId)
                .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND, "Session not found.")))
                .filter(sessionEntity -> sessionEntity.getDueDate().isAfter(LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS)))
                .hasElement();
    }

    private Mono<SessionEntity> verifySessionClosedAndReturn(String sessionId) {
        logger.info("Checking that the session is closed for voting.");
        return sessionRepository.findById(sessionId)
                .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND, "Session not found.")))
                .filter(sessionEntity -> sessionEntity.getDueDate().isBefore(LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS)))
                .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.BAD_REQUEST, "Session is open for voting yet.")));
    }

    private Mono<Boolean> isAssociateAlreadyRegistered(String associateId) {
        logger.info("Checking that the associate is already registered.");
        return associateService.getAssociateById(associateId)
                .hasElement();
    }

    private Mono<Boolean> isAssociateAlreadyVotedForSession(String associateId, String sessionId) {
        logger.info("Checking that the associate already voted for this session.");
        return voteRepository.findByAssociateIdAndSessionId(associateId, sessionId)
                .hasElement();
    }

    private Boolean checkConditionsToVote(Boolean isSessionOpen, Boolean isAssociateAlreadyRegistered, Boolean isAssociateAbleToVote, Boolean isAssociateAlreadyVotedForSession) {
        if (!isSessionOpen) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Session is not open for voting.");
        }
        if (!isAssociateAlreadyRegistered) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Associate is not registered for voting.");
        }
        if (!isAssociateAbleToVote) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Associate is not able for voting.");
        }
        if (isAssociateAlreadyVotedForSession) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Associate already voted for this session.");
        }
        return true;
    }

    private SessionResult countSessionResults(SessionEntity session, List<VoteEntity> votes) {
        return SessionResult.SessionResultsBuilder.aSessionResults()
                .withTopicId(session.getTopicId())
                .withSessionId(session.getId())
                .withTotalVotes(votes.size())
                .withResult(getVotingResult(votes).map(Enum::name).orElse("TIE"))
                .build();
    }

    private Optional<VoteType> getVotingResult(List<VoteEntity> votes) {
        Long totalYesVotes = votes.stream()
                .map(VoteEntity::getValue)
                .filter(voteValue -> voteValue == VoteType.YES)
                .count();
        Long totalNoVotes = votes.stream()
                .map(VoteEntity::getValue)
                .filter(voteValue -> voteValue == VoteType.NO)
                .count();

        if (totalYesVotes > totalNoVotes) {
            return Optional.of(VoteType.YES);
        }
        if (totalNoVotes > totalYesVotes) {
            return Optional.of(VoteType.NO);
        }
        return Optional.empty();
    }
}
