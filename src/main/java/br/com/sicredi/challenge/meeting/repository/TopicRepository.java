package br.com.sicredi.challenge.meeting.repository;

import br.com.sicredi.challenge.meeting.entity.TopicEntity;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TopicRepository extends ReactiveMongoRepository<TopicEntity, String> {
}
