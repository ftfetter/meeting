package br.com.sicredi.challenge.meeting.repository;

import br.com.sicredi.challenge.meeting.entity.AssociateEntity;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface AssociateRepository extends ReactiveMongoRepository<AssociateEntity, String> {

    Mono<AssociateEntity> findByTaxId(String taxId);
}
