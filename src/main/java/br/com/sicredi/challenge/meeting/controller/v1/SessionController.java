package br.com.sicredi.challenge.meeting.controller.v1;

import br.com.sicredi.challenge.meeting.controller.v1.request.SessionRequest;
import br.com.sicredi.challenge.meeting.controller.v1.request.VoteRequest;
import br.com.sicredi.challenge.meeting.controller.v1.response.SessionResponse;
import br.com.sicredi.challenge.meeting.controller.v1.response.SessionResultResponse;
import br.com.sicredi.challenge.meeting.controller.v1.response.VoteResponse;
import br.com.sicredi.challenge.meeting.mapper.SessionMapper;
import br.com.sicredi.challenge.meeting.mapper.SessionResultMapper;
import br.com.sicredi.challenge.meeting.mapper.VoteMapper;
import br.com.sicredi.challenge.meeting.model.Session;
import br.com.sicredi.challenge.meeting.model.Vote;
import br.com.sicredi.challenge.meeting.service.SessionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api/v1/meeting/sessions")
public class SessionController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private SessionService sessionService;

    public SessionController(SessionService sessionService) {
        this.sessionService = sessionService;
    }

    @PostMapping
    public Mono<SessionResponse> createSession(@RequestBody SessionRequest request) {
        logger.info("Request to create a new Session with body: {}", request);
        Session newSession = SessionMapper.fromRequest(request);
        return sessionService.createSession(newSession)
                .map(SessionMapper::toResponse)
                .doOnSuccess(response -> logger.info("Session created successfully: {}", response));
    }

    @PostMapping("/{sessionId}/votes")
    public Mono<VoteResponse> registerSessionVote(@PathVariable String sessionId, @RequestBody VoteRequest request) {
        logger.info("Request to register a new vote for session ID {}: {}", sessionId, request);
        Vote newVote = VoteMapper.fromRequest(sessionId, request);
        return sessionService.registerSessionVote(newVote)
                .map(VoteMapper::toResponse)
                .doOnSuccess(response -> logger.info("Vote registered successfully: {}", response));
    }

    @GetMapping("/{sessionId}/results")
    public Mono<SessionResultResponse> getSessionResults(@PathVariable String sessionId) {
        logger.info("Request to get results of a voting session with ID {}", sessionId);
        return sessionService.getSessionResults(sessionId)
                .map(SessionResultMapper::toResponse)
                .doOnSuccess(response -> logger.info("Voting session results gotten successfully: {}", response));
    }
}
