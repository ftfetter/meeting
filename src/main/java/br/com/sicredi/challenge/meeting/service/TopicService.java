package br.com.sicredi.challenge.meeting.service;

import br.com.sicredi.challenge.meeting.entity.TopicEntity;
import br.com.sicredi.challenge.meeting.mapper.TopicMapper;
import br.com.sicredi.challenge.meeting.model.Topic;
import br.com.sicredi.challenge.meeting.repository.TopicRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class TopicService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private TopicRepository topicRepository;

    public TopicService(TopicRepository topicRepository) {
        this.topicRepository = topicRepository;
    }

    public Mono<Topic> createTopic(Topic newTopic) {
        logger.info("Creating new topic: {}", newTopic);
        TopicEntity newTopicEntity = TopicMapper.toEntity(newTopic);
        return topicRepository.save(newTopicEntity)
                .map(TopicMapper::fromEntity);
    }
}
