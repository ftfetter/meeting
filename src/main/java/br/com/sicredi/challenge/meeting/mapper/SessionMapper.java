package br.com.sicredi.challenge.meeting.mapper;

import br.com.sicredi.challenge.meeting.controller.v1.request.SessionRequest;
import br.com.sicredi.challenge.meeting.controller.v1.response.SessionResponse;
import br.com.sicredi.challenge.meeting.entity.SessionEntity;
import br.com.sicredi.challenge.meeting.model.Session;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

public final class SessionMapper {

    private SessionMapper() {
    }

    public static Session fromRequest(SessionRequest request) {
        return Session.SessionBuilder.aSession()
                .withTopicId(request.getTopicId())
                .withDueDate(LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS).plusMinutes(request.getDurationInMinutes()))
                .build();
    }

    public static SessionEntity toEntity(Session session) {
        return SessionEntity.SessionEntityBuilder.aSessionEntity()
                .withTopicId(session.getTopicId())
                .withDueDate(session.getDueDate())
                .build();
    }

    public static Session fromEntity(SessionEntity entity) {
        return Session.SessionBuilder.aSession()
                .withId(entity.getId())
                .withTopicId(entity.getTopicId())
                .withDueDate(entity.getDueDate())
                .build();
    }

    public static SessionResponse toResponse(Session session) {
        return SessionResponse.SessionResponseBuilder.aSessionResponse()
                .withId(session.getId())
                .withTopicId(session.getTopicId())
                .withDueDate(session.getDueDate())
                .build();
    }
}
