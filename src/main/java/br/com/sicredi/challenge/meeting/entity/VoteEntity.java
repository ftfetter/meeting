package br.com.sicredi.challenge.meeting.entity;

import br.com.sicredi.challenge.meeting.type.VoteType;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Vote")
public class VoteEntity {

    @Id
    private String id;
    private String sessionId;
    private String associateId;
    private VoteType value;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getAssociateId() {
        return associateId;
    }

    public void setAssociateId(String associateId) {
        this.associateId = associateId;
    }

    public VoteType getValue() {
        return value;
    }

    public void setValue(VoteType value) {
        this.value = value;
    }


    public static final class VoteEntityBuilder {
        private String id;
        private String sessionId;
        private String associateId;
        private VoteType value;

        private VoteEntityBuilder() {
        }

        public static VoteEntityBuilder aVoteEntity() {
            return new VoteEntityBuilder();
        }

        public VoteEntityBuilder withId(String id) {
            this.id = id;
            return this;
        }

        public VoteEntityBuilder withSessionId(String sessionId) {
            this.sessionId = sessionId;
            return this;
        }

        public VoteEntityBuilder withAssociateId(String associateId) {
            this.associateId = associateId;
            return this;
        }

        public VoteEntityBuilder withValue(VoteType value) {
            this.value = value;
            return this;
        }

        public VoteEntity build() {
            VoteEntity voteEntity = new VoteEntity();
            voteEntity.setId(id);
            voteEntity.setSessionId(sessionId);
            voteEntity.setAssociateId(associateId);
            voteEntity.setValue(value);
            return voteEntity;
        }
    }
}
