package br.com.sicredi.challenge.meeting.mapper;

import br.com.sicredi.challenge.meeting.controller.v1.request.AssociateRequest;
import br.com.sicredi.challenge.meeting.controller.v1.response.AssociateResponse;
import br.com.sicredi.challenge.meeting.entity.AssociateEntity;
import br.com.sicredi.challenge.meeting.model.Associate;

public final class AssociateMapper {

    private AssociateMapper() {
    }

    public static Associate fromRequest(AssociateRequest request) {
        return Associate.AssociateBuilder.anAssociate()
                .withName(request.getName())
                .withTaxId(request.getTaxId())
                .build();
    }

    public static AssociateResponse toResponse(Associate associate) {
        return AssociateResponse.AssociateResponseBuilder.anAssociateResponse()
                .withId(associate.getId())
                .withName(associate.getName())
                .withTaxId(associate.getTaxId())
                .build();
    }

    public static AssociateEntity toEntity(Associate associate) {
        return AssociateEntity.AssociateEntityBuilder.anAssociateEntity()
                .withName(associate.getName())
                .withTaxId(associate.getTaxId())
                .build();
    }

    public static Associate fromEntity(AssociateEntity entity) {
        return Associate.AssociateBuilder.anAssociate()
                .withId(entity.getId())
                .withName(entity.getName())
                .withTaxId(entity.getTaxId())
                .build();
    }
}
