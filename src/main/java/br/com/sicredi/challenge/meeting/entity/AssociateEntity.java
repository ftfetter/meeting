package br.com.sicredi.challenge.meeting.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Associate")
public class AssociateEntity {

    @Id
    private String id;
    private String name;
    private String taxId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTaxId() {
        return taxId;
    }

    public void setTaxId(String taxId) {
        this.taxId = taxId;
    }


    public static final class AssociateEntityBuilder {
        private String id;
        private String name;
        private String taxId;

        private AssociateEntityBuilder() {
        }

        public static AssociateEntityBuilder anAssociateEntity() {
            return new AssociateEntityBuilder();
        }

        public AssociateEntityBuilder withId(String id) {
            this.id = id;
            return this;
        }

        public AssociateEntityBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public AssociateEntityBuilder withTaxId(String taxId) {
            this.taxId = taxId;
            return this;
        }

        public AssociateEntity build() {
            AssociateEntity associateEntity = new AssociateEntity();
            associateEntity.setId(id);
            associateEntity.setName(name);
            associateEntity.setTaxId(taxId);
            return associateEntity;
        }
    }
}
