package br.com.sicredi.challenge.meeting.controller.v1.response;

public class VoteResponse {

    private String id;
    private String sessionId;
    private String associateId;
    private String value;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getAssociateId() {
        return associateId;
    }

    public void setAssociateId(String associateId) {
        this.associateId = associateId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }


    public static final class VoteResponseBuilder {
        private String id;
        private String sessionId;
        private String associateId;
        private String value;

        private VoteResponseBuilder() {
        }

        public static VoteResponseBuilder aVoteResponse() {
            return new VoteResponseBuilder();
        }

        public VoteResponseBuilder withId(String id) {
            this.id = id;
            return this;
        }

        public VoteResponseBuilder withSessionId(String sessionId) {
            this.sessionId = sessionId;
            return this;
        }

        public VoteResponseBuilder withAssociateId(String associateId) {
            this.associateId = associateId;
            return this;
        }

        public VoteResponseBuilder withValue(String value) {
            this.value = value;
            return this;
        }

        public VoteResponse build() {
            VoteResponse voteResponse = new VoteResponse();
            voteResponse.setId(id);
            voteResponse.setSessionId(sessionId);
            voteResponse.setAssociateId(associateId);
            voteResponse.setValue(value);
            return voteResponse;
        }
    }

    @Override
    public String toString() {
        return "VoteResponse{" +
                "id='" + id + '\'' +
                ", sessionId='" + sessionId + '\'' +
                ", associateId='" + associateId + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
