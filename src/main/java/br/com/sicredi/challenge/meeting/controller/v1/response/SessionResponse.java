package br.com.sicredi.challenge.meeting.controller.v1.response;

import java.time.LocalDateTime;

public class SessionResponse {

    private String id;
    private String topicId;
    private LocalDateTime dueDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTopicId() {
        return topicId;
    }

    public void setTopicId(String topicId) {
        this.topicId = topicId;
    }

    public LocalDateTime getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDateTime dueDate) {
        this.dueDate = dueDate;
    }


    public static final class SessionResponseBuilder {
        private String id;
        private String topicId;
        private LocalDateTime dueDate;

        private SessionResponseBuilder() {
        }

        public static SessionResponseBuilder aSessionResponse() {
            return new SessionResponseBuilder();
        }

        public SessionResponseBuilder withId(String id) {
            this.id = id;
            return this;
        }

        public SessionResponseBuilder withTopicId(String topicId) {
            this.topicId = topicId;
            return this;
        }

        public SessionResponseBuilder withDueDate(LocalDateTime dueDate) {
            this.dueDate = dueDate;
            return this;
        }

        public SessionResponse build() {
            SessionResponse sessionResponse = new SessionResponse();
            sessionResponse.setId(id);
            sessionResponse.setTopicId(topicId);
            sessionResponse.setDueDate(dueDate);
            return sessionResponse;
        }
    }

    @Override
    public String toString() {
        return "SessionResponse{" +
                "id='" + id + '\'' +
                ", topicId='" + topicId + '\'' +
                ", dueDate=" + dueDate +
                '}';
    }
}
