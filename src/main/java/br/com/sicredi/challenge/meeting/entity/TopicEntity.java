package br.com.sicredi.challenge.meeting.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Topic")
public class TopicEntity {

    @Id
    private String id;
    private String name;
    private String description;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public static final class TopicEntityBuilder {
        private String id;
        private String name;
        private String description;

        private TopicEntityBuilder() {
        }

        public static TopicEntityBuilder aTopicEntity() {
            return new TopicEntityBuilder();
        }

        public TopicEntityBuilder withId(String id) {
            this.id = id;
            return this;
        }

        public TopicEntityBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public TopicEntityBuilder withDescription(String description) {
            this.description = description;
            return this;
        }

        public TopicEntity build() {
            TopicEntity topicEntity = new TopicEntity();
            topicEntity.setId(id);
            topicEntity.setName(name);
            topicEntity.setDescription(description);
            return topicEntity;
        }
    }
}
