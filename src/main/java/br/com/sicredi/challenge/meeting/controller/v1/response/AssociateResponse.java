package br.com.sicredi.challenge.meeting.controller.v1.response;

public class AssociateResponse {

    private String id;
    private String name;
    private String taxId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTaxId() {
        return taxId;
    }

    public void setTaxId(String taxId) {
        this.taxId = taxId;
    }


    public static final class AssociateResponseBuilder {
        private String id;
        private String name;
        private String taxId;

        private AssociateResponseBuilder() {
        }

        public static AssociateResponseBuilder anAssociateResponse() {
            return new AssociateResponseBuilder();
        }

        public AssociateResponseBuilder withId(String id) {
            this.id = id;
            return this;
        }

        public AssociateResponseBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public AssociateResponseBuilder withTaxId(String taxId) {
            this.taxId = taxId;
            return this;
        }

        public AssociateResponse build() {
            AssociateResponse associateResponse = new AssociateResponse();
            associateResponse.setId(id);
            associateResponse.setName(name);
            associateResponse.setTaxId(taxId);
            return associateResponse;
        }
    }

    @Override
    public String toString() {
        return "AssociateResponse{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", taxId='" + taxId + '\'' +
                '}';
    }
}
