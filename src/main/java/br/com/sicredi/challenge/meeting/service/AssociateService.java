package br.com.sicredi.challenge.meeting.service;

import br.com.sicredi.challenge.meeting.client.UserInfoClient;
import br.com.sicredi.challenge.meeting.mapper.AssociateMapper;
import br.com.sicredi.challenge.meeting.model.Associate;
import br.com.sicredi.challenge.meeting.repository.AssociateRepository;
import br.com.sicredi.challenge.meeting.type.UserInfoStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Mono;

@Service
public class AssociateService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private AssociateRepository associateRepository;
    private UserInfoClient userInfoClient;

    public AssociateService(AssociateRepository associateRepository, UserInfoClient userInfoClient) {
        this.associateRepository = associateRepository;
        this.userInfoClient = userInfoClient;
    }

    public Mono<Associate> createAssociate(Associate newAssociate) {
        return Mono.zip(
                isAssociateAlreadyCreated(newAssociate.getTaxId()),
                isAbleToVote(newAssociate.getTaxId()))
                .filter(tuple -> !tuple.getT1())
                .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.BAD_REQUEST, "Associate already exists.")))
                .doOnNext(tuple -> logger.info("Creating new associate: {}", newAssociate))
                .map(tuple -> newAssociate)
                .map(AssociateMapper::toEntity)
                .flatMap(newAssociateEntity -> associateRepository.save(newAssociateEntity))
                .map(AssociateMapper::fromEntity);
    }

    public Mono<Associate> getAssociateById(String associateId) {
        logger.info("Searching for associate with ID {}", associateId);
        return associateRepository.findById(associateId)
                .map(AssociateMapper::fromEntity);
    }

    public Mono<Boolean> isAbleToVote(String taxId) {
        logger.info("Checking that the associate can vote.");
        return userInfoClient.getUserInfo(taxId)
                .map(response -> response.getStatus().equalsIgnoreCase(UserInfoStatus.ABLE_TO_VOTE.name()));
    }

    private Mono<Boolean> isAssociateAlreadyCreated(String taxId) {
        logger.info("Checking that the associate is already created.");
        return associateRepository.findByTaxId(taxId)
                .hasElement();
    }
}
