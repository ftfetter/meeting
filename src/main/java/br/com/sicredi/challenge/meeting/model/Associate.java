package br.com.sicredi.challenge.meeting.model;

public class Associate {

    private String id;
    private String name;
    private String taxId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTaxId() {
        return taxId;
    }

    public void setTaxId(String taxId) {
        this.taxId = taxId;
    }


    public static final class AssociateBuilder {
        private String id;
        private String name;
        private String taxId;

        private AssociateBuilder() {
        }

        public static AssociateBuilder anAssociate() {
            return new AssociateBuilder();
        }

        public AssociateBuilder withId(String id) {
            this.id = id;
            return this;
        }

        public AssociateBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public AssociateBuilder withTaxId(String taxId) {
            this.taxId = taxId;
            return this;
        }

        public Associate build() {
            Associate associate = new Associate();
            associate.setId(id);
            associate.setName(name);
            associate.setTaxId(taxId);
            return associate;
        }
    }

    @Override
    public String toString() {
        return "Associate{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", taxId='" + taxId + '\'' +
                '}';
    }
}
