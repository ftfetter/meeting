package br.com.sicredi.challenge.meeting.controller.v1.response;

public class SessionResultResponse {

    private String sessionId;
    private String topicId;
    private Integer totalVotes;
    private String result;

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getTopicId() {
        return topicId;
    }

    public void setTopicId(String topicId) {
        this.topicId = topicId;
    }

    public Integer getTotalVotes() {
        return totalVotes;
    }

    public void setTotalVotes(Integer totalVotes) {
        this.totalVotes = totalVotes;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }


    public static final class SessionResultResponseBuilder {
        private String sessionId;
        private String topicId;
        private Integer totalVotes;
        private String result;

        private SessionResultResponseBuilder() {
        }

        public static SessionResultResponseBuilder aSessionResultResponse() {
            return new SessionResultResponseBuilder();
        }

        public SessionResultResponseBuilder withSessionId(String sessionId) {
            this.sessionId = sessionId;
            return this;
        }

        public SessionResultResponseBuilder withTopicId(String topicId) {
            this.topicId = topicId;
            return this;
        }

        public SessionResultResponseBuilder withTotalVotes(Integer totalVotes) {
            this.totalVotes = totalVotes;
            return this;
        }

        public SessionResultResponseBuilder withResult(String result) {
            this.result = result;
            return this;
        }

        public SessionResultResponse build() {
            SessionResultResponse sessionResultResponse = new SessionResultResponse();
            sessionResultResponse.setSessionId(sessionId);
            sessionResultResponse.setTopicId(topicId);
            sessionResultResponse.setTotalVotes(totalVotes);
            sessionResultResponse.setResult(result);
            return sessionResultResponse;
        }
    }

    @Override
    public String toString() {
        return "SessionResultResponse{" +
                "sessionId='" + sessionId + '\'' +
                ", topicId='" + topicId + '\'' +
                ", totalVotes=" + totalVotes +
                ", result='" + result + '\'' +
                '}';
    }
}
