package br.com.sicredi.challenge.meeting.repository;

import br.com.sicredi.challenge.meeting.entity.VoteEntity;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface VoteRepository extends ReactiveMongoRepository<VoteEntity, String> {

    Mono<VoteEntity> findByAssociateIdAndSessionId(String associateId, String sessionId);
    Flux<VoteEntity> findAllBySessionId(String sessionId);
}
