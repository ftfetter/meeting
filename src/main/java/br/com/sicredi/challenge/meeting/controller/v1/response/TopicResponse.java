package br.com.sicredi.challenge.meeting.controller.v1.response;

public class TopicResponse {

    private String id;
    private String name;
    private String description;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public static final class TopicResponseBuilder {
        private String id;
        private String name;
        private String description;

        private TopicResponseBuilder() {
        }

        public static TopicResponseBuilder aTopicResponse() {
            return new TopicResponseBuilder();
        }

        public TopicResponseBuilder withId(String id) {
            this.id = id;
            return this;
        }

        public TopicResponseBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public TopicResponseBuilder withDescription(String description) {
            this.description = description;
            return this;
        }

        public TopicResponse build() {
            TopicResponse topicResponse = new TopicResponse();
            topicResponse.setId(id);
            topicResponse.setName(name);
            topicResponse.setDescription(description);
            return topicResponse;
        }
    }

    @Override
    public String toString() {
        return "TopicResponse{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
