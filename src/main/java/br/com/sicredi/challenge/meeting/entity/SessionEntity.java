package br.com.sicredi.challenge.meeting.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Document(collection = "Session")
public class SessionEntity {

    @Id
    private String id;
    private String topicId;
    private LocalDateTime dueDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTopicId() {
        return topicId;
    }

    public void setTopicId(String topicId) {
        this.topicId = topicId;
    }

    public LocalDateTime getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDateTime dueDate) {
        this.dueDate = dueDate;
    }


    public static final class SessionEntityBuilder {
        private String id;
        private String topicId;
        private LocalDateTime dueDate;

        private SessionEntityBuilder() {
        }

        public static SessionEntityBuilder aSessionEntity() {
            return new SessionEntityBuilder();
        }

        public SessionEntityBuilder withId(String id) {
            this.id = id;
            return this;
        }

        public SessionEntityBuilder withTopicId(String topicId) {
            this.topicId = topicId;
            return this;
        }

        public SessionEntityBuilder withDueDate(LocalDateTime dueDate) {
            this.dueDate = dueDate;
            return this;
        }

        public SessionEntity build() {
            SessionEntity sessionEntity = new SessionEntity();
            sessionEntity.setId(id);
            sessionEntity.setTopicId(topicId);
            sessionEntity.setDueDate(dueDate);
            return sessionEntity;
        }
    }
}
