package br.com.sicredi.challenge.meeting.client.response;

public class UserInfoClientResponse {

    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
