package br.com.sicredi.challenge.meeting.mapper;

import br.com.sicredi.challenge.meeting.controller.v1.response.VoteResponse;
import br.com.sicredi.challenge.meeting.entity.VoteEntity;
import br.com.sicredi.challenge.meeting.model.Vote;
import br.com.sicredi.challenge.meeting.stub.VoteEntityStub;
import br.com.sicredi.challenge.meeting.stub.VoteRequestStub;
import br.com.sicredi.challenge.meeting.stub.VoteStub;
import br.com.sicredi.challenge.meeting.type.VoteType;
import org.junit.jupiter.api.Test;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.server.ResponseStatusException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

class VoteMapperTest {

    @Test
    void fromRequestShouldReturnVoteWithoutIdAndWithVoteTypeYesFromVoteRequest() {
        Vote actualVote = VoteMapper.fromRequest("TEST_VOTE_REQUEST_SESSIONID", VoteRequestStub.buildVoteRequest("yes"));

        assertNull(actualVote.getId());
        assertEquals("TEST_VOTE_REQUEST_ASSOCIATEID", actualVote.getAssociateId());
        assertEquals("TEST_VOTE_REQUEST_SESSIONID", actualVote.getSessionId());
        assertEquals(VoteType.YES, actualVote.getValue());
    }

    @Test
    void fromRequestShouldReturnVoteWithoutIdAndWithVoteTypeNoFromVoteRequest() {
        Vote actualVote = VoteMapper.fromRequest("TEST_VOTE_REQUEST_SESSIONID", VoteRequestStub.buildVoteRequest("no"));

        assertNull(actualVote.getId());
        assertEquals("TEST_VOTE_REQUEST_ASSOCIATEID", actualVote.getAssociateId());
        assertEquals("TEST_VOTE_REQUEST_SESSIONID", actualVote.getSessionId());
        assertEquals(VoteType.NO, actualVote.getValue());
    }

    @Test
    void fromRequestShouldReturnExceptionWhenVoteValueIsInvalid() {
        assertThrows(ResponseStatusException.class,
                () -> VoteMapper.fromRequest("TEST_VOTE_REQUEST_SESSIONID", VoteRequestStub.buildVoteRequest("foo")));
    }

    @Test
    void toEntityShouldReturnVoteEntityWithoutIdFromVote() {
        VoteType voteType = VoteType.YES;

        VoteEntity actualVoteEntity = VoteMapper.toEntity(VoteStub.buildVoteWithoutId(voteType));

        assertNull(actualVoteEntity.getId());
        assertEquals("TEST_VOTE_ASSOCIATEID", actualVoteEntity.getAssociateId());
        assertEquals("TEST_VOTE_SESSIONID", actualVoteEntity.getSessionId());
        assertEquals(voteType, actualVoteEntity.getValue());
    }

    @Test
    void fromEntityShouldReturnVoteWithIdFromVoteEntity() {
        VoteType voteType = VoteType.YES;

        Vote actualVote = VoteMapper.fromEntity(VoteEntityStub.buildVoteEntity(voteType));

        assertEquals("TEST_VOTE_ENTITY_ID", actualVote.getId());
        assertEquals("TEST_VOTE_ENTITY_ASSOCIATEID", actualVote.getAssociateId());
        assertEquals("TEST_VOTE_ENTITY_SESSIONID", actualVote.getSessionId());
        assertEquals(voteType, actualVote.getValue());
    }

    @Test
    void toResponseShouldReturnVoteResponseFromVote() {
        VoteType voteType = VoteType.YES;

        VoteResponse actualVoteResponse = VoteMapper.toResponse(VoteStub.buildVoteWithId(voteType));

        assertEquals("TEST_VOTE_ID", actualVoteResponse.getId());
        assertEquals("TEST_VOTE_ASSOCIATEID", actualVoteResponse.getAssociateId());
        assertEquals("TEST_VOTE_SESSIONID", actualVoteResponse.getSessionId());
        assertEquals(voteType.name(), actualVoteResponse.getValue());
    }
}