package br.com.sicredi.challenge.meeting.mapper;

import br.com.sicredi.challenge.meeting.controller.v1.response.SessionResponse;
import br.com.sicredi.challenge.meeting.entity.SessionEntity;
import br.com.sicredi.challenge.meeting.model.Session;
import br.com.sicredi.challenge.meeting.stub.SessionEntityStub;
import br.com.sicredi.challenge.meeting.stub.SessionRequestStub;
import br.com.sicredi.challenge.meeting.stub.SessionStub;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;

import static org.junit.jupiter.api.Assertions.*;

class SessionMapperTest {

    @Test
    void fromRequestShouldReturnSessionWithoutIdAndWithDueDateFromRequest() {
        Long durationInMinutes = 120L;
        LocalDateTime currentDateTime = LocalDateTime.now().withSecond(0).withNano(0);

        Session actualSession = SessionMapper.fromRequest(SessionRequestStub.buildSessionRequest(durationInMinutes));

        assertNull(actualSession.getId());
        assertEquals("TEST_SESSION_REQUEST_TOPICID", actualSession.getTopicId());
        assertEquals(currentDateTime.plusMinutes(durationInMinutes), actualSession.getDueDate().withSecond(0).withNano(0));
    }

    @Test
    void toEntityShouldReturnSessionEntityWithoutIdFromSession() {
        LocalDateTime dueDate = LocalDateTime.now();

        SessionEntity actualSessionEntity = SessionMapper.toEntity(SessionStub.buildSessionWithoutId(dueDate));

        assertNull(actualSessionEntity.getId());
        assertEquals("TEST_SESSION_TOPICID", actualSessionEntity.getTopicId());
        assertEquals(dueDate, actualSessionEntity.getDueDate());
    }

    @Test
    void fromEntityShouldReturnSessionFromSessionEntity() {
        LocalDateTime dueDate = LocalDateTime.now();

        Session actualSession = SessionMapper.fromEntity(SessionEntityStub.buildSessionEntity(dueDate));

        assertEquals("TEST_SESSION_ENTITY_ID", actualSession.getId());
        assertEquals("TEST_SESSION_ENTITY_TOPICID", actualSession.getTopicId());
        assertEquals(dueDate, actualSession.getDueDate());
    }

    @Test
    void toResponseShouldReturnSessionResponseFromSession() {
        LocalDateTime dueDate = LocalDateTime.now();

        SessionResponse actualSessionResponse = SessionMapper.toResponse(SessionStub.buildSessionWithId(dueDate));

        assertEquals("TEST_SESSION_ID", actualSessionResponse.getId());
        assertEquals("TEST_SESSION_TOPICID", actualSessionResponse.getTopicId());
        assertEquals(dueDate, actualSessionResponse.getDueDate());
    }
}