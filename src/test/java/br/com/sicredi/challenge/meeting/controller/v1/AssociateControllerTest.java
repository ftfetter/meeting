package br.com.sicredi.challenge.meeting.controller.v1;

import br.com.sicredi.challenge.meeting.client.UserInfoClient;
import br.com.sicredi.challenge.meeting.controller.v1.request.AssociateRequest;
import br.com.sicredi.challenge.meeting.entity.AssociateEntity;
import br.com.sicredi.challenge.meeting.repository.AssociateRepository;
import br.com.sicredi.challenge.meeting.service.AssociateService;
import br.com.sicredi.challenge.meeting.stub.AssociateEntityStub;
import br.com.sicredi.challenge.meeting.stub.AssociateRequestStub;
import br.com.sicredi.challenge.meeting.stub.UserInfoClientResponseStub;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;

import java.io.IOException;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@WebFluxTest(controllers = AssociateController.class)
@Import({AssociateService.class, UserInfoClient.class})
class AssociateControllerTest {

    private static String URI = "/api/v1/meeting/associates";

    @Autowired
    private WebTestClient webTestClient;
    @Autowired
    private ObjectMapper objectMapper;
    @MockBean
    private AssociateRepository associateRepository;

    private MockWebServer mockWebServer;

    @BeforeEach
    void setUp() throws IOException {
        mockWebServer = new MockWebServer();
        mockWebServer.start(8081);
    }

    @AfterEach
    void tearDown() throws IOException {
        mockWebServer.shutdown();
    }

    @Test
    void createAssociateShouldReturnRecentlyCreatedAssociate() throws JsonProcessingException {
        mockWebServer.enqueue(new MockResponse()
                .setBody(objectMapper.writeValueAsString(UserInfoClientResponseStub.buildUserInfoClientResponseAbleToVote()))
                .addHeader("Content-Type", "application/json"));
        when(associateRepository.findByTaxId(anyString()))
                .thenReturn(Mono.empty());
        when(associateRepository.save(any(AssociateEntity.class)))
                .thenReturn(Mono.just(AssociateEntityStub.buildAssociateEntity()));

        webTestClient.post()
                .uri(URI)
                .contentType(MediaType.APPLICATION_JSON)
                .body(Mono.just(AssociateRequestStub.buildAssociateRequest()), AssociateRequest.class)
                .exchange()
                .expectStatus().is2xxSuccessful()
                .expectBody()
                .jsonPath("$.id").isEqualTo("TEST_ASSOCIATE_ENTITY_ID")
                .jsonPath("$.name").isEqualTo("TEST_ASSOCIATE_ENTITY_NAME")
                .jsonPath("$.taxId").isEqualTo("TEST_ASSOCIATE_ENTITY_TAXID");
    }

    @Test
    void createAssociateShouldReturnBadRequestErrorWhenAssociateAlreadyCreated() throws JsonProcessingException {
        mockWebServer.enqueue(new MockResponse()
                .setBody(objectMapper.writeValueAsString(UserInfoClientResponseStub.buildUserInfoClientResponseAbleToVote()))
                .addHeader("Content-Type", "application/json"));
        when(associateRepository.findByTaxId(anyString()))
                .thenReturn(Mono.just(AssociateEntityStub.buildAssociateEntity()));

        webTestClient.post()
                .uri(URI)
                .contentType(MediaType.APPLICATION_JSON)
                .body(Mono.just(AssociateRequestStub.buildAssociateRequest()), AssociateRequest.class)
                .exchange()
                .expectStatus().isBadRequest()
                .expectBody()
                .jsonPath("$.message").isEqualTo("Associate already exists.");
    }

    @Test
    void createAssociateShouldReturnBadRequestErrorWhenAssociateTaxIdNotValid() {
        mockWebServer.enqueue(new MockResponse()
                .setResponseCode(404)
                .addHeader("Content-Type", "application/json"));
        when(associateRepository.findByTaxId(anyString()))
                .thenReturn(Mono.empty());

        webTestClient.post()
                .uri(URI)
                .contentType(MediaType.APPLICATION_JSON)
                .body(Mono.just(AssociateRequestStub.buildAssociateRequest()), AssociateRequest.class)
                .exchange()
                .expectStatus().isBadRequest()
                .expectBody()
                .jsonPath("$.message").isEqualTo("Invalid TaxID.");
    }

    @Test
    void createAssociateShouldReturn5xxErrorWhenCreatingAssociateFails() throws JsonProcessingException {
        mockWebServer.enqueue(new MockResponse()
                .setBody(objectMapper.writeValueAsString(UserInfoClientResponseStub.buildUserInfoClientResponseAbleToVote()))
                .addHeader("Content-Type", "application/json"));
        when(associateRepository.findByTaxId(anyString()))
                .thenReturn(Mono.empty());
        when(associateRepository.save(any(AssociateEntity.class)))
                .thenReturn(Mono.error(new RuntimeException("TEST_ERROR_MESSAGE")));

        webTestClient.post()
                .uri(URI)
                .contentType(MediaType.APPLICATION_JSON)
                .body(Mono.just(AssociateRequestStub.buildAssociateRequest()), AssociateRequest.class)
                .exchange()
                .expectStatus().is5xxServerError()
                .expectBody()
                .jsonPath("$.message").isEqualTo("TEST_ERROR_MESSAGE");
    }
}