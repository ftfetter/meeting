package br.com.sicredi.challenge.meeting.stub;

import br.com.sicredi.challenge.meeting.controller.v1.request.TopicRequest;

public final class TopicRequestStub {

    private TopicRequestStub() {
    }

    public static TopicRequest buildTopicRequest() {
        TopicRequest stub = new TopicRequest();
        stub.setName("TEST_TOPIC_REQUEST_NAME");
        stub.setDescription("TEST_TOPIC_REQUEST_DESCRIPTION");
        return stub;
    }
}
