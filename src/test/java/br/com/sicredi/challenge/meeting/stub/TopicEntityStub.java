package br.com.sicredi.challenge.meeting.stub;

import br.com.sicredi.challenge.meeting.entity.TopicEntity;

public final class TopicEntityStub {

    private TopicEntityStub() {
    }

    public static TopicEntity buildTopicEntity() {
        return TopicEntity.TopicEntityBuilder.aTopicEntity()
                .withId("TEST_TOPIC_ENTITY_ID")
                .withName("TEST_TOPIC_ENTITY_NAME")
                .withDescription("TEST_TOPIC_ENTITY_DESCRIPTION")
                .build();
    }
}
