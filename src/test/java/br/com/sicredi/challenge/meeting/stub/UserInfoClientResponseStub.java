package br.com.sicredi.challenge.meeting.stub;

import br.com.sicredi.challenge.meeting.client.response.UserInfoClientResponse;
import br.com.sicredi.challenge.meeting.type.UserInfoStatus;

public final class UserInfoClientResponseStub {

    private UserInfoClientResponseStub() {
    }

    public static UserInfoClientResponse buildUserInfoClientResponseAbleToVote() {
        UserInfoClientResponse stub = new UserInfoClientResponse();
        stub.setStatus(UserInfoStatus.ABLE_TO_VOTE.name());
        return stub;
    }

    public static UserInfoClientResponse buildUserInfoClientResponseUnableToVote() {
        UserInfoClientResponse stub = new UserInfoClientResponse();
        stub.setStatus(UserInfoStatus.UNABLE_TO_VOTE.name());
        return stub;
    }
}
