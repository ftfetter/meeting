package br.com.sicredi.challenge.meeting.stub;

import br.com.sicredi.challenge.meeting.entity.VoteEntity;
import br.com.sicredi.challenge.meeting.type.VoteType;

import java.util.Arrays;
import java.util.List;

public final class VoteEntityStub {

    private VoteEntityStub() {
    }

    public static VoteEntity buildVoteEntity(VoteType voteType) {
        return VoteEntity.VoteEntityBuilder.aVoteEntity()
                .withId("TEST_VOTE_ENTITY_ID")
                .withAssociateId("TEST_VOTE_ENTITY_ASSOCIATEID")
                .withSessionId("TEST_VOTE_ENTITY_SESSIONID")
                .withValue(voteType)
                .build();
    }

    public static List<VoteEntity> buildVotingResultForYes() {
        return Arrays.asList(
                buildVoteEntity(VoteType.YES),
                buildVoteEntity(VoteType.NO),
                buildVoteEntity(VoteType.YES),
                buildVoteEntity(VoteType.YES),
                buildVoteEntity(VoteType.YES),
                buildVoteEntity(VoteType.NO));
    }

    public static List<VoteEntity> buildVotingResultForNo() {
        return Arrays.asList(
                buildVoteEntity(VoteType.NO),
                buildVoteEntity(VoteType.NO),
                buildVoteEntity(VoteType.YES),
                buildVoteEntity(VoteType.NO),
                buildVoteEntity(VoteType.NO),
                buildVoteEntity(VoteType.YES));
    }

    public static List<VoteEntity> buildVotingResultForTie() {
        return Arrays.asList(
                buildVoteEntity(VoteType.YES),
                buildVoteEntity(VoteType.NO),
                buildVoteEntity(VoteType.YES),
                buildVoteEntity(VoteType.YES),
                buildVoteEntity(VoteType.NO),
                buildVoteEntity(VoteType.NO));
    }
}
