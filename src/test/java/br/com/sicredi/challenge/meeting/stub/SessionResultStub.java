package br.com.sicredi.challenge.meeting.stub;

import br.com.sicredi.challenge.meeting.model.SessionResult;

public final class SessionResultStub {

    private SessionResultStub() {
    }

    public static SessionResult buildSessionResult() {
        return SessionResult.SessionResultsBuilder.aSessionResults()
                .withTopicId("TEST_SESSION_RESULT_TOPICID")
                .withSessionId("TEST_SESSION_RESULT_SESSIONID")
                .withTotalVotes(42)
                .withResult("TEST_SESSION_RESULT_RESULT")
                .build();
    }
}
