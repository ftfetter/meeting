package br.com.sicredi.challenge.meeting.service;

import br.com.sicredi.challenge.meeting.client.UserInfoClient;
import br.com.sicredi.challenge.meeting.entity.AssociateEntity;
import br.com.sicredi.challenge.meeting.repository.AssociateRepository;
import br.com.sicredi.challenge.meeting.stub.AssociateEntityStub;
import br.com.sicredi.challenge.meeting.stub.AssociateStub;
import br.com.sicredi.challenge.meeting.stub.UserInfoClientResponseStub;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class AssociateServiceTest {

    private AssociateService associateService;
    private AssociateRepository associateRepository;
    private UserInfoClient userInfoClient;

    @BeforeEach
    void setUp() {
        associateRepository = mock(AssociateRepository.class);
        userInfoClient = mock(UserInfoClient.class);
        associateService = new AssociateService(associateRepository, userInfoClient);
    }

    @Test
    void createAssociateShouldReturnRecentlyCreatedAssociate() {
        when(associateRepository.findByTaxId(anyString()))
                .thenReturn(Mono.empty());
        when(userInfoClient.getUserInfo(anyString()))
                .thenReturn(Mono.just(UserInfoClientResponseStub.buildUserInfoClientResponseAbleToVote()));
        when(associateRepository.save(any(AssociateEntity.class)))
                .thenReturn(Mono.just(AssociateEntityStub.buildAssociateEntity()));

        StepVerifier.create(associateService.createAssociate(AssociateStub.buildAssociateWithoutId()))
                .assertNext(associate -> assertAll(
                        () -> assertEquals("TEST_ASSOCIATE_ENTITY_ID", associate.getId()),
                        () -> assertEquals("TEST_ASSOCIATE_ENTITY_NAME", associate.getName()),
                        () -> assertEquals("TEST_ASSOCIATE_ENTITY_TAXID", associate.getTaxId())))
                .expectComplete()
                .verify();
    }

    @Test
    void createAssociateShouldThrowExceptionWhenAssociateAlreadyCreated() {
        when(associateRepository.findByTaxId(anyString()))
                .thenReturn(Mono.just(AssociateEntityStub.buildAssociateEntity()));
        when(userInfoClient.getUserInfo(anyString()))
                .thenReturn(Mono.just(UserInfoClientResponseStub.buildUserInfoClientResponseAbleToVote()));

        StepVerifier.create(associateService.createAssociate(AssociateStub.buildAssociateWithoutId()))
                .expectErrorMessage(HttpStatus.BAD_REQUEST.toString() + " \"Associate already exists.\"")
                .verify();
    }

    @Test
    void createAssociateShouldThrowExceptionWhenAssociateTaxIdNotValid() {
        when(associateRepository.findByTaxId(anyString()))
                .thenReturn(Mono.empty());
        when(userInfoClient.getUserInfo(anyString()))
                .thenReturn(Mono.error(new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid TaxID.")));

        StepVerifier.create(associateService.createAssociate(AssociateStub.buildAssociateWithoutId()))
                .expectErrorMessage(HttpStatus.BAD_REQUEST.toString() + " \"Invalid TaxID.\"")
                .verify();
    }

    @Test
    void createAssociateShouldThrowExceptionWhenCreatingAssociateFails() {
        when(associateRepository.findByTaxId(anyString()))
                .thenReturn(Mono.empty());
        when(userInfoClient.getUserInfo(anyString()))
                .thenReturn(Mono.just(UserInfoClientResponseStub.buildUserInfoClientResponseAbleToVote()));
        when(associateRepository.save(any(AssociateEntity.class)))
                .thenReturn(Mono.error(new RuntimeException("TEST_ERROR")));

        StepVerifier.create(associateService.createAssociate(AssociateStub.buildAssociateWithoutId()))
                .expectError(RuntimeException.class)
                .verify();
    }

    @Test
    void getAssociateByIdShouldReturnAssociateWhenExists() {
        when(associateRepository.findById(anyString()))
                .thenReturn(Mono.just(AssociateEntityStub.buildAssociateEntity()));

        StepVerifier.create(associateService.getAssociateById("TEST_ASSOCIATE_ID"))
                .assertNext(associate -> assertAll(
                        () -> assertEquals("TEST_ASSOCIATE_ENTITY_ID", associate.getId()),
                        () -> assertEquals("TEST_ASSOCIATE_ENTITY_NAME", associate.getName()),
                        () -> assertEquals("TEST_ASSOCIATE_ENTITY_TAXID", associate.getTaxId())))
                .expectComplete()
                .verify();
    }

    @Test
    void getAssociateByIdShouldReturnEmptyWhenAssociateDoesNotExists() {
        when(associateRepository.findById(anyString()))
                .thenReturn(Mono.empty());

        StepVerifier.create(associateService.getAssociateById("TEST_ASSOCIATE_ID"))
                .expectNextCount(0)
                .verifyComplete();
    }
}