package br.com.sicredi.challenge.meeting.stub;

import br.com.sicredi.challenge.meeting.controller.v1.request.AssociateRequest;

public final class AssociateRequestStub {

    private AssociateRequestStub() {
    }

    public static AssociateRequest buildAssociateRequest() {
        AssociateRequest stub = new AssociateRequest();
        stub.setName("TEST_ASSOCIATE_REQUEST_NAME");
        stub.setTaxId("TEST_ASSOCIATE_REQUEST_TAXID");
        return stub;
    }
}
