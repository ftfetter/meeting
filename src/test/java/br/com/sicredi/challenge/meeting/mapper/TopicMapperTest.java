package br.com.sicredi.challenge.meeting.mapper;

import br.com.sicredi.challenge.meeting.controller.v1.response.TopicResponse;
import br.com.sicredi.challenge.meeting.entity.TopicEntity;
import br.com.sicredi.challenge.meeting.model.Topic;
import br.com.sicredi.challenge.meeting.stub.TopicEntityStub;
import br.com.sicredi.challenge.meeting.stub.TopicRequestStub;
import br.com.sicredi.challenge.meeting.stub.TopicStub;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class TopicMapperTest {

    @Test
    void fromRequestShouldReturnTopicWithoutIdFromTopicRequest() {
        Topic actualTopic = TopicMapper.fromRequest(TopicRequestStub.buildTopicRequest());

        assertNull(actualTopic.getId());
        assertEquals("TEST_TOPIC_REQUEST_NAME", actualTopic.getName());
        assertEquals("TEST_TOPIC_REQUEST_DESCRIPTION", actualTopic.getDescription());
    }

    @Test
    void toResponseShouldReturnTopicResponseFromTopic() {
        TopicResponse actualTopicResponse = TopicMapper.toResponse(TopicStub.buildTopicWithId());

        assertEquals("TEST_TOPIC_ID", actualTopicResponse.getId());
        assertEquals("TEST_TOPIC_NAME", actualTopicResponse.getName());
        assertEquals("TEST_TOPIC_DESCRIPTION", actualTopicResponse.getDescription());
    }

    @Test
    void fromEntityShouldReturnTopicFromTopicEntity() {
        Topic actualTopic = TopicMapper.fromEntity(TopicEntityStub.buildTopicEntity());

        assertEquals("TEST_TOPIC_ENTITY_ID", actualTopic.getId());
        assertEquals("TEST_TOPIC_ENTITY_NAME", actualTopic.getName());
        assertEquals("TEST_TOPIC_ENTITY_DESCRIPTION", actualTopic.getDescription());
    }

    @Test
    void toEntityShouldReturnTopicEntityWithoutIdFromTopic() {
        TopicEntity actualTopicEntity = TopicMapper.toEntity(TopicStub.buildTopicWithoutId());

        assertNull(actualTopicEntity.getId());
        assertEquals("TEST_TOPIC_NAME", actualTopicEntity.getName());
        assertEquals("TEST_TOPIC_DESCRIPTION", actualTopicEntity.getDescription());
    }
}