package br.com.sicredi.challenge.meeting.stub;

import br.com.sicredi.challenge.meeting.entity.SessionEntity;

import java.time.LocalDateTime;

public final class SessionEntityStub {

    private SessionEntityStub() {
    }

    public static SessionEntity buildSessionEntity(LocalDateTime dueDate) {
        return SessionEntity.SessionEntityBuilder.aSessionEntity()
                .withId("TEST_SESSION_ENTITY_ID")
                .withTopicId("TEST_SESSION_ENTITY_TOPICID")
                .withDueDate(dueDate)
                .build();
    }
}
