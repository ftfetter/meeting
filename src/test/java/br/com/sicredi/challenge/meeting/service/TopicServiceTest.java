package br.com.sicredi.challenge.meeting.service;

import br.com.sicredi.challenge.meeting.entity.TopicEntity;
import br.com.sicredi.challenge.meeting.repository.TopicRepository;
import br.com.sicredi.challenge.meeting.stub.TopicEntityStub;
import br.com.sicredi.challenge.meeting.stub.TopicStub;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class TopicServiceTest {

    private TopicService topicService;
    private TopicRepository topicRepository;

    @BeforeEach
    void setUp() {
        topicRepository = mock(TopicRepository.class);
        topicService = new TopicService(topicRepository);
    }

    @Test
    void createTopicShouldReturnRecentlyCreatedTopic() {
        when(topicRepository.save(any(TopicEntity.class)))
                .thenReturn(Mono.just(TopicEntityStub.buildTopicEntity()));

        StepVerifier.create(topicService.createTopic(TopicStub.buildTopicWithoutId()))
                .assertNext(topic -> assertAll(
                        () -> assertEquals("TEST_TOPIC_ENTITY_ID", topic.getId()),
                        () -> assertEquals("TEST_TOPIC_ENTITY_NAME", topic.getName()),
                        () -> assertEquals("TEST_TOPIC_ENTITY_DESCRIPTION", topic.getDescription())))
                .expectComplete()
                .verify();
    }

    @Test
    void createTopicShouldThrowExceptionWhenCreatingTopicFails() {
        when(topicRepository.save(any(TopicEntity.class)))
                .thenReturn(Mono.error(new RuntimeException("TEST_ERROR")));

        StepVerifier.create(topicService.createTopic(TopicStub.buildTopicWithoutId()))
                .expectError(RuntimeException.class)
                .verify();
    }
}