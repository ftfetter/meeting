package br.com.sicredi.challenge.meeting.mapper;

import br.com.sicredi.challenge.meeting.controller.v1.response.AssociateResponse;
import br.com.sicredi.challenge.meeting.entity.AssociateEntity;
import br.com.sicredi.challenge.meeting.model.Associate;
import br.com.sicredi.challenge.meeting.stub.AssociateEntityStub;
import br.com.sicredi.challenge.meeting.stub.AssociateRequestStub;
import br.com.sicredi.challenge.meeting.stub.AssociateStub;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class AssociateMapperTest {

    @Test
    void fromRequestShouldReturnAssociateWithoutIdFromAssociateRequest() {
        Associate actualAssociate = AssociateMapper.fromRequest(AssociateRequestStub.buildAssociateRequest());

        assertNull(actualAssociate.getId());
        assertEquals("TEST_ASSOCIATE_REQUEST_NAME", actualAssociate.getName());
        assertEquals("TEST_ASSOCIATE_REQUEST_TAXID", actualAssociate.getTaxId());
    }

    @Test
    void toResponseShouldReturnAssociateResponseFromAssociate() {
        AssociateResponse actualAssociateResponse = AssociateMapper.toResponse(AssociateStub.buildAssociateWithId());

        assertEquals("TEST_ASSOCIATE_ID", actualAssociateResponse.getId());
        assertEquals("TEST_ASSOCIATE_NAME", actualAssociateResponse.getName());
        assertEquals("TEST_ASSOCIATE_TAXID", actualAssociateResponse.getTaxId());
    }

    @Test
    void toEntityShouldReturnAssociateEntityWithoutIdFromAssociate() {
        AssociateEntity actualAssociateEntity = AssociateMapper.toEntity(AssociateStub.buildAssociateWithoutId());

        assertNull(actualAssociateEntity.getId());
        assertEquals("TEST_ASSOCIATE_NAME", actualAssociateEntity.getName());
        assertEquals("TEST_ASSOCIATE_TAXID", actualAssociateEntity.getTaxId());
    }

    @Test
    void fromEntityShouldReturnAssociateFromAssociateEntity() {
        Associate actualAssociate = AssociateMapper.fromEntity(AssociateEntityStub.buildAssociateEntity());

        assertEquals("TEST_ASSOCIATE_ENTITY_ID", actualAssociate.getId());
        assertEquals("TEST_ASSOCIATE_ENTITY_NAME", actualAssociate.getName());
        assertEquals("TEST_ASSOCIATE_ENTITY_TAXID", actualAssociate.getTaxId());
    }
}