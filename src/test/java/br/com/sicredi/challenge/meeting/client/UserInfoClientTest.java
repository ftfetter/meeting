package br.com.sicredi.challenge.meeting.client;

import br.com.sicredi.challenge.meeting.stub.UserInfoClientResponseStub;
import br.com.sicredi.challenge.meeting.type.UserInfoStatus;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import reactor.test.StepVerifier;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

class UserInfoClientTest {

    private UserInfoClient userInfoClient;

    private MockWebServer mockWebServer;
    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp() throws IOException {
        userInfoClient = new UserInfoClient("http://localhost:8081");
        objectMapper = new ObjectMapper();
        mockWebServer = new MockWebServer();
        mockWebServer.start(8081);
    }

    @AfterEach
    void tearDown() throws IOException {
        mockWebServer.shutdown();
    }

    @Test
    void getUserInfoShouldReturnUserInfoResponseWithAbleStatus() throws JsonProcessingException {
        mockWebServer.enqueue(new MockResponse()
                .setBody(objectMapper.writeValueAsString(UserInfoClientResponseStub.buildUserInfoClientResponseAbleToVote()))
                .addHeader("Content-Type", "application/json"));

        StepVerifier.create(userInfoClient.getUserInfo("11111111111"))
                .assertNext(response -> assertEquals(UserInfoStatus.ABLE_TO_VOTE.name(), response.getStatus()))
                .expectComplete()
                .verify();
    }

    @Test
    void getUserInfoShouldReturnUserInfoResponseWithUnableStatus() throws JsonProcessingException {
        mockWebServer.enqueue(new MockResponse()
                .setBody(objectMapper.writeValueAsString(UserInfoClientResponseStub.buildUserInfoClientResponseUnableToVote()))
                .addHeader("Content-Type", "application/json"));

        StepVerifier.create(userInfoClient.getUserInfo("11111111111"))
                .assertNext(response -> assertEquals(UserInfoStatus.UNABLE_TO_VOTE.name(), response.getStatus()))
                .expectComplete()
                .verify();
    }

    @Test
    void getUserInfoShouldReturnBadRequestWhenClientReturns404() {
        mockWebServer.enqueue(new MockResponse()
                .setResponseCode(404)
                .addHeader("Content-Type", "application/json"));

        StepVerifier.create(userInfoClient.getUserInfo("11111111111"))
                .expectErrorMessage(HttpStatus.BAD_REQUEST.toString() + " \"Invalid TaxID.\"")
                .verify();
    }
}