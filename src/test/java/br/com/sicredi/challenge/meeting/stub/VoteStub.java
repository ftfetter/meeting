package br.com.sicredi.challenge.meeting.stub;

import br.com.sicredi.challenge.meeting.model.Vote;
import br.com.sicredi.challenge.meeting.type.VoteType;

public final class VoteStub {

    private VoteStub() {
    }

    public static Vote buildVoteWithoutId(VoteType voteType) {
        return Vote.VoteBuilder.aVote()
                .withAssociateId("TEST_VOTE_ASSOCIATEID")
                .withSessionId("TEST_VOTE_SESSIONID")
                .withValue(voteType)
                .build();
    }

    public static Vote buildVoteWithId(VoteType voteType) {
        return Vote.VoteBuilder.aVote()
                .withId("TEST_VOTE_ID")
                .withAssociateId("TEST_VOTE_ASSOCIATEID")
                .withSessionId("TEST_VOTE_SESSIONID")
                .withValue(voteType)
                .build();
    }
}
