package br.com.sicredi.challenge.meeting.stub;

import br.com.sicredi.challenge.meeting.entity.AssociateEntity;

public final class AssociateEntityStub {

    private AssociateEntityStub() {
    }

    public static AssociateEntity buildAssociateEntity() {
        return AssociateEntity.AssociateEntityBuilder.anAssociateEntity()
                .withId("TEST_ASSOCIATE_ENTITY_ID")
                .withName("TEST_ASSOCIATE_ENTITY_NAME")
                .withTaxId("TEST_ASSOCIATE_ENTITY_TAXID")
                .build();
    }
}
