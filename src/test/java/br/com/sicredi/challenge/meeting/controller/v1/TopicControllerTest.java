package br.com.sicredi.challenge.meeting.controller.v1;

import br.com.sicredi.challenge.meeting.controller.v1.request.TopicRequest;
import br.com.sicredi.challenge.meeting.entity.TopicEntity;
import br.com.sicredi.challenge.meeting.repository.TopicRepository;
import br.com.sicredi.challenge.meeting.service.TopicService;
import br.com.sicredi.challenge.meeting.stub.TopicEntityStub;
import br.com.sicredi.challenge.meeting.stub.TopicRequestStub;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@WebFluxTest(controllers = TopicController.class)
@Import(TopicService.class)
class TopicControllerTest {

    private static String URI = "/api/v1/meeting/topics";

    @Autowired
    private WebTestClient webTestClient;
    @MockBean
    private TopicRepository topicRepository;

    @Test
    void createTopicShouldReturnRecentlyCreatedTopic() {
        when(topicRepository.save(any(TopicEntity.class)))
                .thenReturn(Mono.just(TopicEntityStub.buildTopicEntity()));

        webTestClient.post()
                .uri(URI)
                .contentType(MediaType.APPLICATION_JSON)
                .body(Mono.just(TopicRequestStub.buildTopicRequest()), TopicRequest.class)
                .exchange()
                .expectStatus().is2xxSuccessful()
                .expectBody()
                .jsonPath("$.id").isEqualTo("TEST_TOPIC_ENTITY_ID")
                .jsonPath("$.name").isEqualTo("TEST_TOPIC_ENTITY_NAME")
                .jsonPath("$.description").isEqualTo("TEST_TOPIC_ENTITY_DESCRIPTION");
    }

    @Test
    void createTopicShouldReturn5xxErrorWhenCreatingTopicFails() {
        when(topicRepository.save(any(TopicEntity.class)))
                .thenReturn(Mono.error(new RuntimeException("TEST_ERROR_MESSAGE")));

        webTestClient.post()
                .uri(URI)
                .contentType(MediaType.APPLICATION_JSON)
                .body(Mono.just(TopicRequestStub.buildTopicRequest()), TopicRequest.class)
                .exchange()
                .expectStatus().is5xxServerError()
                .expectBody()
                .jsonPath("$.message").isEqualTo("TEST_ERROR_MESSAGE");
    }
}