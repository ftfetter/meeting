package br.com.sicredi.challenge.meeting.controller.v1;

import br.com.sicredi.challenge.meeting.client.UserInfoClient;
import br.com.sicredi.challenge.meeting.controller.v1.request.SessionRequest;
import br.com.sicredi.challenge.meeting.controller.v1.request.VoteRequest;
import br.com.sicredi.challenge.meeting.entity.SessionEntity;
import br.com.sicredi.challenge.meeting.entity.VoteEntity;
import br.com.sicredi.challenge.meeting.repository.AssociateRepository;
import br.com.sicredi.challenge.meeting.repository.SessionRepository;
import br.com.sicredi.challenge.meeting.repository.VoteRepository;
import br.com.sicredi.challenge.meeting.service.AssociateService;
import br.com.sicredi.challenge.meeting.service.SessionService;
import br.com.sicredi.challenge.meeting.stub.AssociateEntityStub;
import br.com.sicredi.challenge.meeting.stub.SessionEntityStub;
import br.com.sicredi.challenge.meeting.stub.SessionRequestStub;
import br.com.sicredi.challenge.meeting.stub.UserInfoClientResponseStub;
import br.com.sicredi.challenge.meeting.stub.VoteEntityStub;
import br.com.sicredi.challenge.meeting.stub.VoteRequestStub;
import br.com.sicredi.challenge.meeting.type.VoteType;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@WebFluxTest(controllers = SessionController.class)
@Import({SessionService.class, AssociateService.class, UserInfoClient.class})
class SessionControllerTest {

    private static String URI = "/api/v1/meeting/sessions";

    @Autowired
    private WebTestClient webTestClient;
    @Autowired
    private ObjectMapper objectMapper;
    @MockBean
    private SessionRepository sessionRepository;
    @MockBean
    private VoteRepository voteRepository;
    @MockBean
    private AssociateRepository associateRepository;

    private MockWebServer mockWebServer;

    @BeforeEach
    void setUp() throws IOException {
        mockWebServer = new MockWebServer();
        mockWebServer.start(8081);
    }

    @AfterEach
    void tearDown() throws IOException {
        mockWebServer.shutdown();
    }

    @Test
    void createSessionShouldReturnRecentlyCreatedSession() {
        LocalDateTime dueDate = LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS);
        when(sessionRepository.save(any(SessionEntity.class)))
                .thenReturn(Mono.just(SessionEntityStub.buildSessionEntity(dueDate)));

        webTestClient.post()
                .uri(URI)
                .contentType(MediaType.APPLICATION_JSON)
                .body(Mono.just(SessionRequestStub.buildSessionRequest(120L)), SessionRequest.class)
                .exchange()
                .expectStatus().is2xxSuccessful()
                .expectBody()
                .jsonPath("$.id").isEqualTo("TEST_SESSION_ENTITY_ID")
                .jsonPath("$.topicId").isEqualTo("TEST_SESSION_ENTITY_TOPICID")
                .jsonPath("$.dueDate").isEqualTo(dueDate.toString());
    }

    @Test
    void createSessionShouldReturn5xxErrorWhenCreatingSessionFails() {
        when(sessionRepository.save(any(SessionEntity.class)))
                .thenReturn(Mono.error(new RuntimeException("TEST_ERROR_MESSAGE")));

        webTestClient.post()
                .uri(URI)
                .contentType(MediaType.APPLICATION_JSON)
                .body(Mono.just(SessionRequestStub.buildSessionRequest(120L)), SessionRequest.class)
                .exchange()
                .expectStatus().is5xxServerError()
                .expectBody()
                .jsonPath("$.message").isEqualTo("TEST_ERROR_MESSAGE");
    }

    @Test
    void registerSessionVoteShouldReturnRecentlyRegisteredVote() throws JsonProcessingException {
        LocalDateTime notExpiredDueDate = LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS).plusMinutes(120);
        mockWebServer.enqueue(new MockResponse()
                .setBody(objectMapper.writeValueAsString(UserInfoClientResponseStub.buildUserInfoClientResponseAbleToVote()))
                .addHeader("Content-Type", "application/json"));
        when(sessionRepository.findById(anyString()))
                .thenReturn(Mono.just(SessionEntityStub.buildSessionEntity(notExpiredDueDate)));
        when(associateRepository.findById(anyString()))
                .thenReturn(Mono.just(AssociateEntityStub.buildAssociateEntity()));
        when(voteRepository.findByAssociateIdAndSessionId(anyString(), anyString()))
                .thenReturn(Mono.empty());
        when(voteRepository.save(any(VoteEntity.class)))
                .thenReturn(Mono.just(VoteEntityStub.buildVoteEntity(VoteType.YES)));

        webTestClient.post()
                .uri(URI + "/" + "TEST_VOTE_REQUEST_SESSIONID" + "/votes")
                .contentType(MediaType.APPLICATION_JSON)
                .body(Mono.just(VoteRequestStub.buildVoteRequest("yes")), VoteRequest.class)
                .exchange()
                .expectStatus().is2xxSuccessful()
                .expectBody()
                .jsonPath("$.id").isEqualTo("TEST_VOTE_ENTITY_ID")
                .jsonPath("$.associateId").isEqualTo("TEST_VOTE_ENTITY_ASSOCIATEID")
                .jsonPath("$.sessionId").isEqualTo("TEST_VOTE_ENTITY_SESSIONID")
                .jsonPath("$.value").isEqualTo(VoteType.YES.name());
    }

    @Test
    void registerSessionVoteShouldReturnBadRequestWhenSessionIsClosedForVoting() throws JsonProcessingException {
        LocalDateTime expiredDueDate = LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS).minusMinutes(120);
        mockWebServer.enqueue(new MockResponse()
                .setBody(objectMapper.writeValueAsString(UserInfoClientResponseStub.buildUserInfoClientResponseAbleToVote()))
                .addHeader("Content-Type", "application/json"));
        when(sessionRepository.findById(anyString()))
                .thenReturn(Mono.just(SessionEntityStub.buildSessionEntity(expiredDueDate)));
        when(associateRepository.findById(anyString()))
                .thenReturn(Mono.just(AssociateEntityStub.buildAssociateEntity()));
        when(voteRepository.findByAssociateIdAndSessionId(anyString(), anyString()))
                .thenReturn(Mono.empty());

        webTestClient.post()
                .uri(URI + "/" + "TEST_VOTE_REQUEST_SESSIONID" + "/votes")
                .contentType(MediaType.APPLICATION_JSON)
                .body(Mono.just(VoteRequestStub.buildVoteRequest("yes")), VoteRequest.class)
                .exchange()
                .expectStatus().isBadRequest()
                .expectBody()
                .jsonPath("$.message").isEqualTo("Session is not open for voting.");
    }

    @Test
    void registerSessionVoteShouldReturnBadRequestWhenSessionNotFound() throws JsonProcessingException {
        mockWebServer.enqueue(new MockResponse()
                .setBody(objectMapper.writeValueAsString(UserInfoClientResponseStub.buildUserInfoClientResponseAbleToVote()))
                .addHeader("Content-Type", "application/json"));
        when(sessionRepository.findById(anyString()))
                .thenReturn(Mono.empty());
        when(associateRepository.findById(anyString()))
                .thenReturn(Mono.just(AssociateEntityStub.buildAssociateEntity()));
        when(voteRepository.findByAssociateIdAndSessionId(anyString(), anyString()))
                .thenReturn(Mono.empty());

        webTestClient.post()
                .uri(URI + "/" + "TEST_VOTE_REQUEST_SESSIONID" + "/votes")
                .contentType(MediaType.APPLICATION_JSON)
                .body(Mono.just(VoteRequestStub.buildVoteRequest("yes")), VoteRequest.class)
                .exchange()
                .expectStatus().isNotFound()
                .expectBody()
                .jsonPath("$.message").isEqualTo("Session not found.");
    }

    @Test
    void registerSessionVoteShouldReturnBadRequestWhenAssociateDoesNotExists() throws JsonProcessingException {
        LocalDateTime notExpiredDueDate = LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS).plusMinutes(120);
        mockWebServer.enqueue(new MockResponse()
                .setBody(objectMapper.writeValueAsString(UserInfoClientResponseStub.buildUserInfoClientResponseAbleToVote()))
                .addHeader("Content-Type", "application/json"));
        when(sessionRepository.findById(anyString()))
                .thenReturn(Mono.just(SessionEntityStub.buildSessionEntity(notExpiredDueDate)));
        when(associateRepository.findById(anyString()))
                .thenReturn(Mono.empty());
        when(voteRepository.findByAssociateIdAndSessionId(anyString(), anyString()))
                .thenReturn(Mono.empty());

        webTestClient.post()
                .uri(URI + "/" + "TEST_VOTE_REQUEST_SESSIONID" + "/votes")
                .contentType(MediaType.APPLICATION_JSON)
                .body(Mono.just(VoteRequestStub.buildVoteRequest("yes")), VoteRequest.class)
                .exchange()
                .expectStatus().isBadRequest()
                .expectBody()
                .jsonPath("$.message").isEqualTo("Associate is not registered for voting.");
    }

    @Test
    void registerSessionVoteShouldReturnBadRequestWhenAssociateUnableToVote() throws JsonProcessingException {
        LocalDateTime notExpiredDueDate = LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS).plusMinutes(120);
        mockWebServer.enqueue(new MockResponse()
                .setBody(objectMapper.writeValueAsString(UserInfoClientResponseStub.buildUserInfoClientResponseUnableToVote()))
                .addHeader("Content-Type", "application/json"));
        when(sessionRepository.findById(anyString()))
                .thenReturn(Mono.just(SessionEntityStub.buildSessionEntity(notExpiredDueDate)));
        when(associateRepository.findById(anyString()))
                .thenReturn(Mono.just(AssociateEntityStub.buildAssociateEntity()));
        when(voteRepository.findByAssociateIdAndSessionId(anyString(), anyString()))
                .thenReturn(Mono.empty());

        webTestClient.post()
                .uri(URI + "/" + "TEST_VOTE_REQUEST_SESSIONID" + "/votes")
                .contentType(MediaType.APPLICATION_JSON)
                .body(Mono.just(VoteRequestStub.buildVoteRequest("yes")), VoteRequest.class)
                .exchange()
                .expectStatus().isBadRequest()
                .expectBody()
                .jsonPath("$.message").isEqualTo("Associate is not able for voting.");
    }

    @Test
    void registerSessionVoteShouldReturnBadRequestWhenAssociateAlreadyVotedForCurrentSession() throws JsonProcessingException {
        LocalDateTime notExpiredDueDate = LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS).plusMinutes(120);
        mockWebServer.enqueue(new MockResponse()
                .setBody(objectMapper.writeValueAsString(UserInfoClientResponseStub.buildUserInfoClientResponseAbleToVote()))
                .addHeader("Content-Type", "application/json"));
        when(sessionRepository.findById(anyString()))
                .thenReturn(Mono.just(SessionEntityStub.buildSessionEntity(notExpiredDueDate)));
        when(associateRepository.findById(anyString()))
                .thenReturn(Mono.just(AssociateEntityStub.buildAssociateEntity()));
        when(voteRepository.findByAssociateIdAndSessionId(anyString(), anyString()))
                .thenReturn(Mono.just(VoteEntityStub.buildVoteEntity(VoteType.YES)));

        webTestClient.post()
                .uri(URI + "/" + "TEST_VOTE_REQUEST_SESSIONID" + "/votes")
                .contentType(MediaType.APPLICATION_JSON)
                .body(Mono.just(VoteRequestStub.buildVoteRequest("yes")), VoteRequest.class)
                .exchange()
                .expectStatus().isBadRequest()
                .expectBody()
                .jsonPath("$.message").isEqualTo("Associate already voted for this session.");
    }

    @Test
    void registerSessionVoteShouldReturn5xxErrorWhenRegisteringVoteFails() throws JsonProcessingException {
        LocalDateTime notExpiredDueDate = LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS).plusMinutes(120);
        mockWebServer.enqueue(new MockResponse()
                .setBody(objectMapper.writeValueAsString(UserInfoClientResponseStub.buildUserInfoClientResponseAbleToVote()))
                .addHeader("Content-Type", "application/json"));
        when(sessionRepository.findById(anyString()))
                .thenReturn(Mono.just(SessionEntityStub.buildSessionEntity(notExpiredDueDate)));
        when(associateRepository.findById(anyString()))
                .thenReturn(Mono.just(AssociateEntityStub.buildAssociateEntity()));
        when(voteRepository.findByAssociateIdAndSessionId(anyString(), anyString()))
                .thenReturn(Mono.empty());
        when(voteRepository.save(any(VoteEntity.class)))
                .thenReturn(Mono.error(new RuntimeException("TEST_ERROR_MESSAGE")));

        webTestClient.post()
                .uri(URI + "/" + "TEST_VOTE_REQUEST_SESSIONID" + "/votes")
                .contentType(MediaType.APPLICATION_JSON)
                .body(Mono.just(VoteRequestStub.buildVoteRequest("yes")), VoteRequest.class)
                .exchange()
                .expectStatus().is5xxServerError()
                .expectBody()
                .jsonPath("$.message").isEqualTo("TEST_ERROR_MESSAGE");
    }

    @Test
    void getSessionResultsShouldReturnSessionResultWithYesForTheGivenSessionId() {
        LocalDateTime expiredDueDate = LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS).minusMinutes(120);
        when(sessionRepository.findById(anyString()))
                .thenReturn(Mono.just(SessionEntityStub.buildSessionEntity(expiredDueDate)));
        when(voteRepository.findAllBySessionId(anyString()))
                .thenReturn(Flux.fromIterable(VoteEntityStub.buildVotingResultForYes()));

        webTestClient.get()
                .uri(URI + "/" + "TEST_SESSION_ID" + "/results")
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.topicId").isEqualTo("TEST_SESSION_ENTITY_TOPICID")
                .jsonPath("$.sessionId").isEqualTo("TEST_SESSION_ENTITY_ID")
                .jsonPath("$.totalVotes").isEqualTo("6")
                .jsonPath("$.result").isEqualTo("YES");
    }

    @Test
    void getSessionResultsShouldReturnSessionResultWithNoForTheGivenSessionId() {
        LocalDateTime expiredDueDate = LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS).minusMinutes(120);
        when(sessionRepository.findById(anyString()))
                .thenReturn(Mono.just(SessionEntityStub.buildSessionEntity(expiredDueDate)));
        when(voteRepository.findAllBySessionId(anyString()))
                .thenReturn(Flux.fromIterable(VoteEntityStub.buildVotingResultForNo()));

        webTestClient.get()
                .uri(URI + "/" + "TEST_SESSION_ID" + "/results")
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.topicId").isEqualTo("TEST_SESSION_ENTITY_TOPICID")
                .jsonPath("$.sessionId").isEqualTo("TEST_SESSION_ENTITY_ID")
                .jsonPath("$.totalVotes").isEqualTo("6")
                .jsonPath("$.result").isEqualTo("NO");
    }

    @Test
    void getSessionResultsShouldReturnSessionResultWithTieForTheGivenSessionId() {
        LocalDateTime expiredDueDate = LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS).minusMinutes(120);
        when(sessionRepository.findById(anyString()))
                .thenReturn(Mono.just(SessionEntityStub.buildSessionEntity(expiredDueDate)));
        when(voteRepository.findAllBySessionId(anyString()))
                .thenReturn(Flux.fromIterable(VoteEntityStub.buildVotingResultForTie()));

        webTestClient.get()
                .uri(URI + "/" + "TEST_SESSION_ID" + "/results")
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.topicId").isEqualTo("TEST_SESSION_ENTITY_TOPICID")
                .jsonPath("$.sessionId").isEqualTo("TEST_SESSION_ENTITY_ID")
                .jsonPath("$.totalVotes").isEqualTo("6")
                .jsonPath("$.result").isEqualTo("TIE");
    }

    @Test
    void getSessionResultsShouldReturnSessionResultWithTieForTheGivenSessionIdWhenVotesNotFound() {
        LocalDateTime expiredDueDate = LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS).minusMinutes(120);
        when(sessionRepository.findById(anyString()))
                .thenReturn(Mono.just(SessionEntityStub.buildSessionEntity(expiredDueDate)));
        when(voteRepository.findAllBySessionId(anyString()))
                .thenReturn(Flux.empty());

        webTestClient.get()
                .uri(URI + "/" + "TEST_SESSION_ID" + "/results")
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.topicId").isEqualTo("TEST_SESSION_ENTITY_TOPICID")
                .jsonPath("$.sessionId").isEqualTo("TEST_SESSION_ENTITY_ID")
                .jsonPath("$.totalVotes").isEqualTo("0")
                .jsonPath("$.result").isEqualTo("TIE");
    }

    @Test
    void getSessionResultsShouldReturnBadRequestWhenSessionIsNotClose() {
        LocalDateTime notExpiredDueDate = LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS).plusMinutes(120);
        when(sessionRepository.findById(anyString()))
                .thenReturn(Mono.just(SessionEntityStub.buildSessionEntity(notExpiredDueDate)));
        when(voteRepository.findAllBySessionId(anyString()))
                .thenReturn(Flux.fromIterable(VoteEntityStub.buildVotingResultForYes()));

        webTestClient.get()
                .uri(URI + "/" + "TEST_SESSION_ID" + "/results")
                .exchange()
                .expectStatus().isBadRequest()
                .expectBody()
                .jsonPath("$.message").isEqualTo("Session is open for voting yet.");
    }

    @Test
    void getSessionResultsShouldReturnNotFoundWhenSessionNotFound() {
        when(sessionRepository.findById(anyString()))
                .thenReturn(Mono.empty());
        when(voteRepository.findAllBySessionId(anyString()))
                .thenReturn(Flux.fromIterable(VoteEntityStub.buildVotingResultForYes()));

        webTestClient.get()
                .uri(URI + "/" + "TEST_SESSION_ID" + "/results")
                .exchange()
                .expectStatus().isNotFound()
                .expectBody()
                .jsonPath("$.message").isEqualTo("Session not found.");
    }
}