package br.com.sicredi.challenge.meeting.stub;

import br.com.sicredi.challenge.meeting.controller.v1.request.SessionRequest;

public final class SessionRequestStub {

    private SessionRequestStub() {
    }

    public static SessionRequest buildSessionRequest(Long durationInMinutes) {
        SessionRequest stub = new SessionRequest();
        stub.setTopicId("TEST_SESSION_REQUEST_TOPICID");
        stub.setDurationInMinutes(durationInMinutes);
        return stub;
    }
}
