package br.com.sicredi.challenge.meeting.stub;

import br.com.sicredi.challenge.meeting.model.Associate;

public final class AssociateStub {

    private AssociateStub() {
    }

    public static Associate buildAssociateWithoutId() {
        return Associate.AssociateBuilder.anAssociate()
                .withName("TEST_ASSOCIATE_NAME")
                .withTaxId("TEST_ASSOCIATE_TAXID")
                .build();
    }

    public static Associate buildAssociateWithId() {
        return Associate.AssociateBuilder.anAssociate()
                .withId("TEST_ASSOCIATE_ID")
                .withName("TEST_ASSOCIATE_NAME")
                .withTaxId("TEST_ASSOCIATE_TAXID")
                .build();
    }
}
