package br.com.sicredi.challenge.meeting.mapper;

import br.com.sicredi.challenge.meeting.controller.v1.response.SessionResultResponse;
import br.com.sicredi.challenge.meeting.stub.SessionResultStub;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SessionResultMapperTest {

    @Test
    void toResponseShouldReturnSessionResultResponseFromSessionResult() {
        SessionResultResponse actualSessionResultResponse = SessionResultMapper.toResponse(SessionResultStub.buildSessionResult());

        assertEquals("TEST_SESSION_RESULT_TOPICID", actualSessionResultResponse.getTopicId());
        assertEquals("TEST_SESSION_RESULT_SESSIONID", actualSessionResultResponse.getSessionId());
        assertEquals(42, actualSessionResultResponse.getTotalVotes());
        assertEquals("TEST_SESSION_RESULT_RESULT", actualSessionResultResponse.getResult());
    }
}