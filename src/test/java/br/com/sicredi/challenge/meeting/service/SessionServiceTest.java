package br.com.sicredi.challenge.meeting.service;

import br.com.sicredi.challenge.meeting.entity.SessionEntity;
import br.com.sicredi.challenge.meeting.entity.VoteEntity;
import br.com.sicredi.challenge.meeting.repository.SessionRepository;
import br.com.sicredi.challenge.meeting.repository.VoteRepository;
import br.com.sicredi.challenge.meeting.stub.AssociateStub;
import br.com.sicredi.challenge.meeting.stub.SessionEntityStub;
import br.com.sicredi.challenge.meeting.stub.SessionStub;
import br.com.sicredi.challenge.meeting.stub.VoteEntityStub;
import br.com.sicredi.challenge.meeting.stub.VoteStub;
import br.com.sicredi.challenge.meeting.type.VoteType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class SessionServiceTest {

    private SessionService sessionService;
    private SessionRepository sessionRepository;
    private VoteRepository voteRepository;
    private AssociateService associateService;

    @BeforeEach
    void setUp() {
        sessionRepository = mock(SessionRepository.class);
        voteRepository = mock(VoteRepository.class);
        associateService = mock(AssociateService.class);
        sessionService = new SessionService(sessionRepository, voteRepository, associateService);
    }

    @Test
    void createSessionShouldReturnRecentlyCreatedSession() {
        LocalDateTime dueDate = LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS);
        when(sessionRepository.save(any(SessionEntity.class)))
                .thenReturn(Mono.just(SessionEntityStub.buildSessionEntity(dueDate)));

        StepVerifier.create(sessionService.createSession(SessionStub.buildSessionWithoutId(dueDate)))
                .assertNext(session -> assertAll(
                        () -> assertEquals("TEST_SESSION_ENTITY_ID", session.getId()),
                        () -> assertEquals("TEST_SESSION_ENTITY_TOPICID", session.getTopicId()),
                        () -> assertEquals(dueDate, session.getDueDate())))
                .expectComplete()
                .verify();
    }

    @Test
    void createSessionShouldThrowExceptionWhenCreatingSessionFails() {
        when(sessionRepository.save(any(SessionEntity.class)))
                .thenReturn(Mono.error(new RuntimeException("TEST_ERROR")));

        StepVerifier.create(sessionService.createSession(SessionStub.buildSessionWithoutId(LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS))))
                .expectError(RuntimeException.class)
                .verify();
    }

    @Test
    void registerSessionVoteShouldReturnRecentlyRegisteredVote() {
        LocalDateTime notExpiredDueDate = LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS).plusMinutes(120);
        when(sessionRepository.findById(anyString()))
                .thenReturn(Mono.just(SessionEntityStub.buildSessionEntity(notExpiredDueDate)));
        when(associateService.getAssociateById(anyString()))
                .thenReturn(Mono.just(AssociateStub.buildAssociateWithId()));
        when(associateService.isAbleToVote(anyString()))
                .thenReturn(Mono.just(true));
        when(voteRepository.findByAssociateIdAndSessionId(anyString(), anyString()))
                .thenReturn(Mono.empty());
        when(voteRepository.save(any(VoteEntity.class)))
                .thenReturn(Mono.just(VoteEntityStub.buildVoteEntity(VoteType.YES)));

        StepVerifier.create(sessionService.registerSessionVote(VoteStub.buildVoteWithoutId(VoteType.YES)))
                .assertNext(vote -> assertAll(
                        () -> assertEquals("TEST_VOTE_ENTITY_ID", vote.getId()),
                        () -> assertEquals("TEST_VOTE_ENTITY_ASSOCIATEID", vote.getAssociateId()),
                        () -> assertEquals("TEST_VOTE_ENTITY_SESSIONID", vote.getSessionId()),
                        () -> assertEquals(VoteType.YES, vote.getValue())))
                .expectComplete()
                .verify();
    }

    @Test
    void registerSessionVoteShouldThrowExceptionWhenSessionIsClosedForVoting() {
        LocalDateTime expiredDueDate = LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS).minusMinutes(120);
        when(sessionRepository.findById(anyString()))
                .thenReturn(Mono.just(SessionEntityStub.buildSessionEntity(expiredDueDate)));
        when(associateService.getAssociateById(anyString()))
                .thenReturn(Mono.just(AssociateStub.buildAssociateWithId()));
        when(associateService.isAbleToVote(anyString()))
                .thenReturn(Mono.just(true));
        when(voteRepository.findByAssociateIdAndSessionId(anyString(), anyString()))
                .thenReturn(Mono.empty());

        StepVerifier.create(sessionService.registerSessionVote(VoteStub.buildVoteWithoutId(VoteType.YES)))
                .expectErrorMessage(HttpStatus.BAD_REQUEST.toString().concat(" \"Session is not open for voting.\""))
                .verify();
    }

    @Test
    void registerSessionVoteShouldThrowExceptionWhenSessionDoesNotExists() {
        when(sessionRepository.findById(anyString()))
                .thenReturn(Mono.empty());
        when(associateService.getAssociateById(anyString()))
                .thenReturn(Mono.just(AssociateStub.buildAssociateWithId()));
        when(associateService.isAbleToVote(anyString()))
                .thenReturn(Mono.just(true));
        when(voteRepository.findByAssociateIdAndSessionId(anyString(), anyString()))
                .thenReturn(Mono.empty());

        StepVerifier.create(sessionService.registerSessionVote(VoteStub.buildVoteWithoutId(VoteType.YES)))
                .expectErrorMessage(HttpStatus.NOT_FOUND.toString().concat(" \"Session not found.\""))
                .verify();
    }

    @Test
    void registerSessionVoteShouldThrowExceptionWhenAssociateDoesNotExists() {
        LocalDateTime notExpiredDueDate = LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS).plusMinutes(120);
        when(sessionRepository.findById(anyString()))
                .thenReturn(Mono.just(SessionEntityStub.buildSessionEntity(notExpiredDueDate)));
        when(associateService.getAssociateById(anyString()))
                .thenReturn(Mono.empty());
        when(associateService.isAbleToVote(anyString()))
                .thenReturn(Mono.just(true));
        when(voteRepository.findByAssociateIdAndSessionId(anyString(), anyString()))
                .thenReturn(Mono.empty());

        StepVerifier.create(sessionService.registerSessionVote(VoteStub.buildVoteWithoutId(VoteType.YES)))
                .expectErrorMessage(HttpStatus.BAD_REQUEST.toString().concat(" \"Associate is not registered for voting.\""))
                .verify();
    }

    @Test
    void registerSessionVoteShouldThrowExceptionWhenAssociateUnableToVote() {
        LocalDateTime notExpiredDueDate = LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS).plusMinutes(120);
        when(sessionRepository.findById(anyString()))
                .thenReturn(Mono.just(SessionEntityStub.buildSessionEntity(notExpiredDueDate)));
        when(associateService.getAssociateById(anyString()))
                .thenReturn(Mono.just(AssociateStub.buildAssociateWithId()));
        when(associateService.isAbleToVote(anyString()))
                .thenReturn(Mono.just(false));
        when(voteRepository.findByAssociateIdAndSessionId(anyString(), anyString()))
                .thenReturn(Mono.empty());

        StepVerifier.create(sessionService.registerSessionVote(VoteStub.buildVoteWithoutId(VoteType.YES)))
                .expectErrorMessage(HttpStatus.BAD_REQUEST.toString().concat(" \"Associate is not able for voting.\""))
                .verify();
    }

    @Test
    void registerSessionVoteShouldThrowExceptionWhenAssociateAlreadyVotedForCurrentSession() {
        LocalDateTime notExpiredDueDate = LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS).plusMinutes(120);
        when(sessionRepository.findById(anyString()))
                .thenReturn(Mono.just(SessionEntityStub.buildSessionEntity(notExpiredDueDate)));
        when(associateService.getAssociateById(anyString()))
                .thenReturn(Mono.just(AssociateStub.buildAssociateWithId()));
        when(associateService.isAbleToVote(anyString()))
                .thenReturn(Mono.just(true));
        when(voteRepository.findByAssociateIdAndSessionId(anyString(), anyString()))
                .thenReturn(Mono.just(VoteEntityStub.buildVoteEntity(VoteType.YES)));

        StepVerifier.create(sessionService.registerSessionVote(VoteStub.buildVoteWithoutId(VoteType.YES)))
                .expectErrorMessage(HttpStatus.BAD_REQUEST.toString().concat(" \"Associate already voted for this session.\""))
                .verify();
    }

    @Test
    void registerSessionVoteShouldThrowExceptionWhenRegisteringVoteFails() {
        LocalDateTime notExpiredDueDate = LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS).plusMinutes(120);
        when(sessionRepository.findById(anyString()))
                .thenReturn(Mono.just(SessionEntityStub.buildSessionEntity(notExpiredDueDate)));
        when(associateService.getAssociateById(anyString()))
                .thenReturn(Mono.just(AssociateStub.buildAssociateWithId()));
        when(associateService.isAbleToVote(anyString()))
                .thenReturn(Mono.just(true));
        when(voteRepository.findByAssociateIdAndSessionId(anyString(), anyString()))
                .thenReturn(Mono.empty());
        when(voteRepository.save(any(VoteEntity.class)))
                .thenReturn(Mono.error(new RuntimeException("TEST_ERROR")));

        StepVerifier.create(sessionService.registerSessionVote(VoteStub.buildVoteWithoutId(VoteType.YES)))
                .expectError(RuntimeException.class)
                .verify();
    }

    @Test
    void getSessionResultsShouldReturnGivenSessionResultsWithYes() {
        LocalDateTime expiredDueDate = LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS).minusMinutes(120);
        when(sessionRepository.findById(anyString()))
                .thenReturn(Mono.just(SessionEntityStub.buildSessionEntity(expiredDueDate)));
        when(voteRepository.findAllBySessionId(anyString()))
                .thenReturn(Flux.fromIterable(VoteEntityStub.buildVotingResultForYes()));

        StepVerifier.create(sessionService.getSessionResults("TEST_SESSION_ID"))
                .assertNext(sessionResults -> assertAll(
                        () -> assertEquals("TEST_SESSION_ENTITY_TOPICID", sessionResults.getTopicId()),
                        () -> assertEquals("TEST_SESSION_ENTITY_ID", sessionResults.getSessionId()),
                        () -> assertEquals(6, sessionResults.getTotalVotes()),
                        () -> assertEquals(VoteType.YES.name(), sessionResults.getResult())))
                .expectComplete()
                .verify();
    }

    @Test
    void getSessionResultsShouldReturnGivenSessionResultsWithNo() {
        LocalDateTime expiredDueDate = LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS).minusMinutes(120);
        when(sessionRepository.findById(anyString()))
                .thenReturn(Mono.just(SessionEntityStub.buildSessionEntity(expiredDueDate)));
        when(voteRepository.findAllBySessionId(anyString()))
                .thenReturn(Flux.fromIterable(VoteEntityStub.buildVotingResultForNo()));

        StepVerifier.create(sessionService.getSessionResults("TEST_SESSION_ID"))
                .assertNext(sessionResults -> assertAll(
                        () -> assertEquals("TEST_SESSION_ENTITY_TOPICID", sessionResults.getTopicId()),
                        () -> assertEquals("TEST_SESSION_ENTITY_ID", sessionResults.getSessionId()),
                        () -> assertEquals(6, sessionResults.getTotalVotes()),
                        () -> assertEquals(VoteType.NO.name(), sessionResults.getResult())))
                .expectComplete()
                .verify();
    }

    @Test
    void getSessionResultsShouldReturnGivenSessionResultsWithTie() {
        LocalDateTime expiredDueDate = LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS).minusMinutes(120);
        when(sessionRepository.findById(anyString()))
                .thenReturn(Mono.just(SessionEntityStub.buildSessionEntity(expiredDueDate)));
        when(voteRepository.findAllBySessionId(anyString()))
                .thenReturn(Flux.fromIterable(VoteEntityStub.buildVotingResultForTie()));

        StepVerifier.create(sessionService.getSessionResults("TEST_SESSION_ID"))
                .assertNext(sessionResults -> assertAll(
                        () -> assertEquals("TEST_SESSION_ENTITY_TOPICID", sessionResults.getTopicId()),
                        () -> assertEquals("TEST_SESSION_ENTITY_ID", sessionResults.getSessionId()),
                        () -> assertEquals(6, sessionResults.getTotalVotes()),
                        () -> assertEquals("TIE", sessionResults.getResult())))
                .expectComplete()
                .verify();
    }

    @Test
    void getSessionResultsShouldReturnGivenSessionResultsWithTieWhenVotesNotFound() {
        LocalDateTime expiredDueDate = LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS).minusMinutes(120);
        when(sessionRepository.findById(anyString()))
                .thenReturn(Mono.just(SessionEntityStub.buildSessionEntity(expiredDueDate)));
        when(voteRepository.findAllBySessionId(anyString()))
                .thenReturn(Flux.empty());

        StepVerifier.create(sessionService.getSessionResults("TEST_SESSION_ID"))
                .assertNext(sessionResults -> assertAll(
                        () -> assertEquals("TEST_SESSION_ENTITY_TOPICID", sessionResults.getTopicId()),
                        () -> assertEquals("TEST_SESSION_ENTITY_ID", sessionResults.getSessionId()),
                        () -> assertEquals(0, sessionResults.getTotalVotes()),
                        () -> assertEquals("TIE", sessionResults.getResult())))
                .expectComplete()
                .verify();
    }

    @Test
    void getSessionResultsShouldThrowExceptionWhenSessionIsNotClosed() {
        LocalDateTime notExpiredDueDate = LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS).plusMinutes(120);
        when(sessionRepository.findById(anyString()))
                .thenReturn(Mono.just(SessionEntityStub.buildSessionEntity(notExpiredDueDate)));
        when(voteRepository.findAllBySessionId(anyString()))
                .thenReturn(Flux.fromIterable(VoteEntityStub.buildVotingResultForYes()));

        StepVerifier.create(sessionService.getSessionResults("TEST_SESSION_ID"))
                .expectErrorMessage(HttpStatus.BAD_REQUEST.toString().concat(" \"Session is open for voting yet.\""))
                .verify();
    }

    @Test
    void getSessionResultsShouldThrowExceptionWhenSessionNotFound() {
        when(sessionRepository.findById(anyString()))
                .thenReturn(Mono.empty());
        when(voteRepository.findAllBySessionId(anyString()))
                .thenReturn(Flux.fromIterable(VoteEntityStub.buildVotingResultForYes()));

        StepVerifier.create(sessionService.getSessionResults("TEST_SESSION_ID"))
                .expectErrorMessage(HttpStatus.NOT_FOUND.toString().concat(" \"Session not found.\""))
                .verify();
    }
}