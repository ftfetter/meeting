package br.com.sicredi.challenge.meeting.stub;

import br.com.sicredi.challenge.meeting.controller.v1.request.VoteRequest;

public final class VoteRequestStub {

    private VoteRequestStub() {
    }

    public static VoteRequest buildVoteRequest(String voteValue) {
        VoteRequest stub = new VoteRequest();
        stub.setAssociateId("TEST_VOTE_REQUEST_ASSOCIATEID");
        stub.setValue(voteValue);
        return stub;
    }
}
