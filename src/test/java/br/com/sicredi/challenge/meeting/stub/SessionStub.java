package br.com.sicredi.challenge.meeting.stub;

import br.com.sicredi.challenge.meeting.model.Session;

import java.time.LocalDateTime;

public final class SessionStub {

    private SessionStub() {
    }

    public static Session buildSessionWithoutId(LocalDateTime dueDate) {
        return Session.SessionBuilder.aSession()
                .withTopicId("TEST_SESSION_TOPICID")
                .withDueDate(dueDate)
                .build();
    }

    public static Session buildSessionWithId(LocalDateTime dueDate) {
        return Session.SessionBuilder.aSession()
                .withId("TEST_SESSION_ID")
                .withTopicId("TEST_SESSION_TOPICID")
                .withDueDate(dueDate)
                .build();
    }
}
