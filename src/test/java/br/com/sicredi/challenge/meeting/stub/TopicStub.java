package br.com.sicredi.challenge.meeting.stub;

import br.com.sicredi.challenge.meeting.model.Topic;

public final class TopicStub {

    private TopicStub() {
    }

    public static Topic buildTopicWithoutId() {
        return Topic.TopicBuilder.aTopic()
                .withName("TEST_TOPIC_NAME")
                .withDescription("TEST_TOPIC_DESCRIPTION")
                .build();
    }

    public static Topic buildTopicWithId() {
        return Topic.TopicBuilder.aTopic()
                .withId("TEST_TOPIC_ID")
                .withName("TEST_TOPIC_NAME")
                .withDescription("TEST_TOPIC_DESCRIPTION")
                .build();
    }
}
